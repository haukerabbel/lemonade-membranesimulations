#include<iostream>
#include<cstring>
#include <unistd.h>
#include <set>

#include <LeMonADE/core/Ingredients.h>
#include <LeMonADE/core/Molecules.h>
#include <LeMonADE/core/ConfigureSystem.h>
#include <LeMonADE/feature/FeatureMoleculesIO.h>
#include <LeMonADE/feature/FeatureAttributes.h>
#include <LeMonADE/feature/FeatureNNInteractionSc.h>
#include <LeMonADE/feature/FeatureLatticePowerOfTwo.h>
#include <LeMonADE/updater/UpdaterReadBfmFile.h>
#include <LeMonADE/utility/RandomNumberGenerators.h>
#include <LeMonADE/utility/Vector3D.h>
#include <LeMonADE/utility/TaskManager.h>
#include <LeMonADE/updater/UpdaterSynchronize.h>

#include <LeMonADE-Extensions/utilities/CommandlineParser.h>
#include <LeMonADE-Extensions/features/FeatureChargedMonomers.h>

#include<LeMonADE-MembraneSimulations/analyzer/AnalyzerBilayerProfile.h>
#include<LeMonADE-MembraneSimulations/analyzer/AnalyzerPolymerPMF.h>
#include<LeMonADE-MembraneSimulations/analyzer/AnalyzerBilayerChargeDensity.h>
#include<LeMonADE-MembraneSimulations/analyzer/AnalyzerLipidOrderParameter.h>
#include<LeMonADE-MembraneSimulations/analyzer/AnalyzerLipidOrderParameterAroundPolymer.h>
#include<LeMonADE-MembraneSimulations/analyzer/AnalyzerPolymerOrderParameter.h>
#include<LeMonADE-MembraneSimulations/updaters/UpdaterRetagBilayerGPU.h>


//// main program //////////////////////////////////////////////////////////////

int main(int argc, char* argv[])
{
	
	
	std::string inputFilename(argv[1]);
	
	try{	
		//seed global random number generators
		RandomNumberGenerators randomNumbers;
		randomNumbers.seedAll();
		
		//////////////add command line arguments /////////////////////////////////
		std::vector<std::string> filenames;
		double binWidth=1.0;
		int32_t minMcs=1000000; //1e6
		
		
		CommandLineParser cmd;
		cmd.addOption("--help",0,"display help");
		cmd.addOption("--gpu",0,"retag monomers for analysis of gpu simulation data");
		cmd.addOption("--charge",0,"add analyzer for charge density");
		cmd.addOption("--pmf",2,"analyze pmf for polymer of types given, give two arguments here");
		cmd.addOption("--profile",0,"analyze bilayer profile");
		cmd.addOption("--lipid_order",0,"analyze lipid order parameter tensor");
		cmd.addOption("--lipid_order_polymer",1,"analyze lipid order parameter tensor around polymer with distance given as argument");
		cmd.addOption("--polymer_surface_order",0,"analyze polymer contacts with different monomer types");
		cmd.addOption("--min_mcs",1,"smallest mcs to analyse");
		
		if ( argc < 2 || std::string(argv[1])=="--help" ){std::cout<<"usage: program outputFilename [options]\n";cmd.displayHelp();exit(0);}
		
		filenames=cmd.getInputFiles(argv,argc);
		if(filenames.size()==0){
			std::cerr<<"need at least one filename\n";
			cmd.displayHelp();
			exit(0);
		}
		else
			std::cout<<"processing "<<filenames.size()<<" files\n";
		
		cmd.parse(argv+filenames.size()+1,argc-filenames.size()-1);
		//get options here
		if(cmd.getOption("--help")){std::cerr<<"usage: program filename [options]\n";cmd.displayHelp();exit(0);}
		cmd.getOption("--min_mcs",minMcs);
		
		
		//define ingredients
		typedef LOKI_TYPELIST_4(FeatureMoleculesIO,FeatureAttributes,FeatureChargedMonomers,FeatureNNInteractionSc<FeatureLatticePowerOfTwo>) Features;
		typedef ConfigureSystem<VectorInt3,Features> Config;
		typedef Ingredients<Config> Ing;
		Ing myIngredients;
		
		TaskManager taskManager;
		
		taskManager.addUpdater(new UpdaterReadBfmFile<Ing>(inputFilename,myIngredients,UpdaterReadBfmFile<Ing>::READ_STEPWISE));
		
		if(cmd.getOption("--gpu"))
			taskManager.addUpdater(new UpdaterRetagBilayerGPU<Ing>(myIngredients));
		
		if(cmd.getOption("--lipid_order"))
			taskManager.addAnalyzer(new AnalyzerLipidOrderParameter<Ing>(myIngredients,minMcs));
		if(cmd.getOption("--lipid_order_polymer")){
			double radius; cmd.getOption("--lipid_order_polymer",radius);
			taskManager.addAnalyzer(new AnalyzerLipidOrderParameterAroundPolymer<Ing>(myIngredients,radius,minMcs));
		}
		if(cmd.getOption("--profile"))
			taskManager.addAnalyzer(new AnalyzerBilayerProfile<Ing>(myIngredients,1.0,minMcs));
		if(cmd.getOption("--polymer_surface_order"))
			taskManager.addAnalyzer(new AnalyzerPolymerOrderParameter<Ing>(myIngredients,"polymerInterfaceOrderParameter.dat",minMcs));
		if(cmd.getOption("--charge"))
			taskManager.addAnalyzer(new AnalyzerBilayerChargeDensity<Ing>(myIngredients,1.0,minMcs));
		if(cmd.getOption("--pmf")){
			std::set<int32_t> types;int32_t t; 
			cmd.getOption("--pmf",t,0); types.insert(t);
			cmd.getOption("--pmf",t,1); types.insert(t);
			taskManager.addAnalyzer(new AnalyzerPolymerPMF<Ing>(myIngredients,
									    types,
						       AnalyzerPolymerPMF<Ing>::ALL_COM,
						       "polymerPMF.dat",
						       minMcs));
		}
		/*init and run the analysis*/
		taskManager.initialize();
		taskManager.run();
		taskManager.cleanup();
	}
	catch(std::runtime_error& e){std::cerr<<e.what()<<std::endl;}
	catch(std::exception& e){std::cerr<<e.what()<<std::endl;}
	
	return 0;
}