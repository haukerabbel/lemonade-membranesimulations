#include<iostream>
#include<cstring>
#include <unistd.h>

#include <LeMonADE/core/Ingredients.h>
#include <LeMonADE/core/Molecules.h>
#include <LeMonADE/core/ConfigureSystem.h>
#include <LeMonADE/feature/FeatureMoleculesIO.h>
#include <LeMonADE/feature/FeatureNNInteractionSc.h>
#include <LeMonADE/feature/FeatureLatticePowerOfTwo.h>
#include <LeMonADE/analyzer/AnalyzerWriteBfmFile.h>
#include <LeMonADE/updater/UpdaterSimpleSimulator.h>
#include <LeMonADE/updater/UpdaterReadBfmFile.h>
#include <LeMonADE/updater/UpdaterSynchronize.h>
#include <LeMonADE/utility/RandomNumberGenerators.h>
#include <LeMonADE/utility/Vector3D.h>
#include <LeMonADE/utility/TaskManager.h>
#include <LeMonADE/analyzer/AnalyzerWriteBfmFileSubGroup.h>

#include <LeMonADE-Extensions/updaters/UpdaterFillSolvent.h>
#include <LeMonADE-Extensions/updaters/UpdaterLinearChainCreator.h>
#include <LeMonADE-Extensions/updaters/UpdaterBasicSimulator.h>
#include <LeMonADE-Extensions/analyzers/LatticeOccupationAnalyzer.h>
#include <LeMonADE-Extensions/analyzers/UnmovedMonomersAnalyzer.h>
#include <LeMonADE-Extensions/utilities/IteratorPredicates.h>
#include <LeMonADE-Extensions/utilities/CommandlineParser.h>
#include <LeMonADE-Extensions/features/FeatureDebyeHuckel.h>

#include <LeMonADE-MembraneSimulations/updaters/BilayerCreator.h>
#include <LeMonADE-MembraneSimulations/updaters/ChargeLipids.h>
#include <LeMonADE-MembraneSimulations/updaters/AddCounterions.h>
#include <LeMonADE-MembraneSimulations/updaters/ChargeMonomerGroup.h>
#include <LeMonADE-MembraneSimulations/updaters/UpdaterHydrophobicitySetup.h>
#include <LeMonADE-MembraneSimulations/updaters/UpdaterCreateChargedHP.h>
#include <LeMonADE-MembraneSimulations/updaters/UpdaterCreateChargedABCP.h>
#include <LeMonADE-MembraneSimulations/updaters/UpdaterCreateChargedRCP.h>
#include <LeMonADE-MembraneSimulations/utilities/MonomerTypes.h>
#include <LeMonADE-MembraneSimulations/analyzer/AnalyzerTranslocationProfile.h>
//// main program //////////////////////////////////////////////////////////////

int main(int argc, char* argv[])
{
	////////////////////default values for command line parameters /////////////
	
	uint64_t upper_simulation_limit=100000000; //1e8mcs
	uint64_t save_interval=50000;
	uint32_t equilibration_steps=10000000; //1e7mcs
	
	int32_t box_x=64;
	int32_t box_y=64;
	int32_t box_z=64;
	
	//lipid definitions
	int32_t n_lipids=300;
	uint32_t nAnionicLipids=0;
	uint32_t nZwitterionicLipids=0;
	
	//counterions definitions
	bool chooseNoCounterions=false; //lets the user explicit choose the number of counterions
	uint32_t nCounterAnions=0;
	uint32_t nCounterCations=0;
	
	//electrostatic interaction settings
	//defaults correspond to a=1.54 angstöm, l_b=0.7nm, l_D=1nm
	double bjerrumLength=4.55;//a
	double kappa=0.154; //1/a
	double debyeCutoffRadius=20.0; //default is set in synchronize of FeatureDebyeHuckel
	double debyeVerletSkin=0.0; //used only if set to larger 1.0
	bool shiftPotential=true; //shift debye potential to 0 at cutoff radius
	
	//contact interaction settings
	std::vector<double> hydrophobicy(8,0.0); //hydrophobicities of polymer types A,B
	hydrophobicy[MonomerTypesCPU::LIPID_TAIL-1]=1.0;
	hydrophobicy[MonomerTypesCPU::LIPID_HEAD-1]=0.0;
	hydrophobicy[MonomerTypesCPU::SOLVENT-1]=0.0;
	hydrophobicy[MonomerTypesCPU::POLYMER_A-1]=0.0;
	hydrophobicy[MonomerTypesCPU::POLYMER_B-1]=1.0;
	hydrophobicy[MonomerTypesCPU::COUNTERION_A-1]=0.0;
	hydrophobicy[MonomerTypesCPU::COUNTERION_B-1]=0.0;
	hydrophobicy[MonomerTypesCPU::TYPE_C-1]=0.0;
	
	//input/output settings
	bool new_system=true; //flag set to false if input config is given
	bool writeSolvent=false; 
	std::string inputFilename;
	std::string outputFilename;
	
	
	
	try{	
		//seed global random number generators
		RandomNumberGenerators randomNumbers;
		randomNumbers.seedAll();
		
		//////////////add command line arguments /////////////////////////////////
		CommandLineParser cmd;
		cmd.addOption("--max_mcs",1,"simulation runs to this mcs (default 1e8)");
		cmd.addOption("--save_interval",1,"mcs interval for saving configurations (default 50000)");
		cmd.addOption("--equilibration",1,"mcs for equilibration (default 1e7)");	
		cmd.addOption("--box",3,"size of simulation box. --box x y z. default 64 64 64");
		cmd.addOption("--nlipids",1,"number of lipids in the simulation box (default 300)");
		cmd.addOption("--a_lipids",1,"number of anionic lipids out of nlipids, default 0");
		cmd.addOption("--z_lipids",1,"number of zwitterionic lipids out of nlipids, default 0");
		cmd.addOption("--nions",2,"number of explicit ions used. syntax --nions anions cations");
		cmd.addOption("--hp",4,"add homopolymers, syntax: type,length,charge,nPolymers");
		cmd.addOption("--abcp",8,"add ABCP, syntax: tA,bA,cA,tB,bB,cB,nRep,nPoly, makes nPoly molecules (tA_bA tB_bB)_nRep with c* charges on blocks");
		cmd.addOption("--rcp",7,"add RCP, syntax: tA,nA,cA,tB,nB,cB,nPoly, with cA/B charge of singe A/B monomers, nA/B total number of such monomers");
		cmd.addOption("--bjerrum_length",1,"set bjerrum length. default 4.55 lattice units");
		cmd.addOption("--kappa",1,"set inverse debye length. default 0.154/lattic unit");
		cmd.addOption("--debye_cutoff",1,"set debye potential cutoff. default 20");
		cmd.addOption("--no_shift_potential",0,"unset shift debye potential to 0 at cutoff distance");
		cmd.addOption("--use_verlet",1,"use verlet list for debye potential.\n\t\t argument is verlet skin in lattice units. 6 could be a reasonable value.");
		cmd.addOption("--hydrophobicity",8,
			      "adjust relative hydrophobicity of types 1-8.\n\t\t syntax --hydrophobicity H_1...H_8. default 1.0 0.0 0.0 0.0 1.0 0.0 0.0 0.0\n\t\ttail head solvent A B IonA IonB C");
		cmd.addOption("--startconfig",1,"use the given file as start configuration. other conflicting options (e.g. box) are ignored");
		cmd.addOption("--write_solvent",0,"enable continuous writing of the solvent coordinates in the output file");		
		cmd.addOption("--solvent_permeability",1,"measure solvent permeability, argument specifies boundaries for measurement.");
		cmd.addOption("--ion_permeability",1,"measure counterion permeability, argument specifies boundaries for measurement.");
		cmd.addOption("--help",0,"display help message");
		
		if ( argc < 2 || std::string(argv[1])=="--help" ){std::cout<<"usage: program outputFilename [options]\n";cmd.displayHelp();exit(0);}
		
		//get output filename as first argument
		outputFilename=std::string(argv[1]);
		
		
		//////////////parse command line arguments /////////////////////////////////
		cmd.parse(argv+2,argc-2);
		
		cmd.getOption("--max_mcs",upper_simulation_limit);
		cmd.getOption("--save_interval",save_interval);
		cmd.getOption("--equilibration",equilibration_steps);
		cmd.getOption("--box",box_x,0);
		cmd.getOption("--box",box_y,1);
		cmd.getOption("--box",box_z,2);		
		cmd.getOption("--nlipids",n_lipids);
		cmd.getOption("--a_lipids",nAnionicLipids);
		cmd.getOption("--z_lipids",nZwitterionicLipids);
		chooseNoCounterions=cmd.getOption("--nions",nCounterAnions,0);
		cmd.getOption("--nions",nCounterCations,1);
		cmd.getOption("--bjerrum_length",bjerrumLength);
		cmd.getOption("--kappa",kappa);
		cmd.getOption("--debye_cutoff",debyeCutoffRadius);
		shiftPotential=!( cmd.getOption("--no_shift_potential"));
		cmd.getOption("--use_verlet",debyeVerletSkin);    
		for(size_t n=0;n<8;n++)
			cmd.getOption("--hydrophobicity",hydrophobicy[n],n);
		new_system=!(cmd.getOption("--startconfig",inputFilename));
		writeSolvent=cmd.getOption("--write_solvent");
		
		if(cmd.getOption("--help")){std::cerr<<"usage: program filename [options]\n";cmd.displayHelp();exit(0);}
		
		//////////// done getting command line parameters....start setting up the system ///
		///////////////////////////////////////////////////////////////////////
		
		
		//define ingredients
		typedef LOKI_TYPELIST_3(FeatureMoleculesIO,FeatureNNInteractionSc<FeatureLatticePowerOfTwo>,FeatureDebyeHuckel) Features;
		typedef ConfigureSystem<VectorInt3,Features> Config;
		typedef Ingredients<Config> Ing;
		Ing myIngredients;
		
		
		TaskManager taskManager;
		
		//////// check validity of lattice occupation during setup and simulation ///
		LatticeOccupationAnalyzer<Ing>* latticeCheck=new LatticeOccupationAnalyzer<Ing>(myIngredients);
		
		//if new system flag was set, we construct a new system with the given parameters //
		if(new_system)
		{
			/////////// set system parameters //////////////////////////////////////
			/*set up the box*/
			myIngredients.setBoxX(box_x);
			myIngredients.setBoxY(box_y);
			myIngredients.setBoxZ(box_z);
			myIngredients.setPeriodicX(1);
			myIngredients.setPeriodicY(1);
			myIngredients.setPeriodicZ(1);
			/*add the bondset*/
			myIngredients.modifyBondset().addBFMclassicBondset();
			
			//////// set electrostatic interactions ////////////////////////////////
			myIngredients.setBjerrumLength(bjerrumLength);
			myIngredients.setKappa(kappa);
			
			if(debyeCutoffRadius>0.0)
				myIngredients.setCutoffRadius(debyeCutoffRadius);
			if(debyeVerletSkin>1.0)
			{
				myIngredients.setUseVerletList(true);
				myIngredients.setVerletSkin(debyeVerletSkin);
			}
			
			/*synchronize to create lattice*/
			myIngredients.synchronize(myIngredients);
			
			latticeCheck->initialize();
			
			///////////// fill the system with monomers ////////////////////////////
			/*define the hydrophobic interactions*/
			taskManager.addUpdater(new UpdaterHydrophobicitySetup<Ing>(myIngredients,hydrophobicy),0);
			/*create the lipid bilayer*/
			taskManager.addUpdater(new BilayerCreator<Ing>(myIngredients,n_lipids),0);
			/*charge the lipids*/
			taskManager.addUpdater(new ChargeLipids<Ing>(myIngredients,nAnionicLipids,nZwitterionicLipids),0);
			/*now put homopolymer in the system */
			if(cmd.getOption("--hp")){
				int32_t type; cmd.getOption("--hp",type,0);
				uint32_t length; cmd.getOption("--hp",length,1);
				int32_t charge; cmd.getOption("--hp",charge,2);
				uint32_t nPolymers; cmd.getOption("--hp",nPolymers,3);
				taskManager.addUpdater(new UpdaterCreateChargedHP<Ing>(myIngredients,type,length,charge,nPolymers),0);
			}
			/*now put alternating block copolymer in the system */
			if(cmd.getOption("--abcp")){
				int32_t typeA,typeB;
				uint32_t blockSizeA,blockSizeB,nBlocks,nPolymers;
				int32_t chargeBlockA,chargeBlockB;
				cmd.getOption("--abcp",typeA,0); 
				cmd.getOption("--abcp",blockSizeA,1);
				cmd.getOption("--abcp",chargeBlockA,2);
				cmd.getOption("--abcp",typeB,3); 
				cmd.getOption("--abcp",blockSizeB,4);
				cmd.getOption("--abcp",chargeBlockB,5);
				cmd.getOption("--abcp",nBlocks,6);
				cmd.getOption("--abcp",nPolymers,7);
				taskManager.addUpdater(new UpdaterCreateChargedABCP<Ing>(myIngredients,
											 typeA,typeB,blockSizeA,blockSizeB,
							     chargeBlockA,chargeBlockB,
							     nBlocks,nPolymers),0);
			}
			/*add random copolymers in the system*/
			if(cmd.getOption("--rcp")){
				int32_t typeA,typeB;
				uint32_t nMonosA,nMonosB,nPolymers;
				int32_t monoChargeA,monoChargeB;
				cmd.getOption("--rcp",typeA,0); 
				cmd.getOption("--rcp",nMonosA,1);
				cmd.getOption("--rcp",monoChargeA,2);
				cmd.getOption("--rcp",typeB,3); 
				cmd.getOption("--rcp",nMonosB,4);
				cmd.getOption("--rcp",monoChargeB,5);
				cmd.getOption("--rcp",nPolymers,6);
				taskManager.addUpdater(new UpdaterCreateChargedRCP<Ing>(myIngredients,
											typeA,typeB,nMonosA,nMonosB,
							    monoChargeA,monoChargeB,
							    nPolymers),0);
			}
			/*add the counterions to the system*/
			if(chooseNoCounterions==false)
			{
				//first determine the necessary number coming from charged lipids
				nCounterAnions=nZwitterionicLipids;
				nCounterCations=nCounterAnions+nAnionicLipids;
				//add the number coming from charged polymers
				if(cmd.getOption("--hp")){
					int32_t HPcharges;cmd.getOption("--hp",HPcharges,2);
					int32_t nPolymers;cmd.getOption("--hp",nPolymers,3);
					if(HPcharges>0) nCounterAnions+=nPolymers*HPcharges;
					if(HPcharges<0) nCounterCations+=uint32_t(-nPolymers*HPcharges);
				}
				if(cmd.getOption("--abcp")){
					int32_t aCharges;cmd.getOption("--abcp",aCharges,2);
					int32_t bCharges;cmd.getOption("--abcp",bCharges,5);
					int32_t nBlocks;cmd.getOption("--abcp",nBlocks,6);
					int32_t nPolymers;cmd.getOption("--abcp",nPolymers,7);
					if(aCharges>0) nCounterAnions+=nPolymers*nBlocks*aCharges;
					if(aCharges<0) nCounterCations+=uint32_t(-nPolymers*nBlocks*aCharges);
					if(bCharges>0) nCounterAnions+=nPolymers*nBlocks*bCharges;
					if(bCharges<0) nCounterCations+=uint32_t(-nPolymers*nBlocks*bCharges);
				}
				if(cmd.getOption("--rcp")){
					int32_t nMonosA;cmd.getOption("--rcp",nMonosA,1);
					int32_t chargeA;cmd.getOption("--rcp",chargeA,2);
					int32_t nMonosB;cmd.getOption("--rcp",nMonosB,4);
					int32_t chargeB;cmd.getOption("--rcp",chargeB,5);
					
					int32_t nPolymers;cmd.getOption("--rcp",nPolymers,6);
					if(chargeA>0) nCounterAnions+=nPolymers*nMonosA*chargeA;
					if(chargeA<0) nCounterCations+=uint32_t(-nPolymers*nMonosA*chargeA);
					if(chargeB>0) nCounterAnions+=nPolymers*nMonosB*chargeB;
					if(chargeB<0) nCounterCations+=uint32_t(-nPolymers*nMonosB*chargeB);
				}
			}
			//else the numbers were given as a command line argument
			taskManager.addUpdater(new AddCounterions<Ing>(myIngredients,nCounterCations,1,MonomerTypesCPU::COUNTERION_A),0);
			taskManager.addUpdater(new AddCounterions<Ing>(myIngredients,nCounterAnions,-1,MonomerTypesCPU::COUNTERION_B),0);
			
			/*fill the system with solvent*/
			double solventDesity=0.5;
			taskManager.addUpdater(new UpdaterFillSolvent<Ing>(myIngredients,0.5),0);
			
		}
		else
		{
			std::cout<<"using file "<<inputFilename<<" as start configuration. output file is "<<outputFilename<<std::endl;
			UpdaterReadBfmFile<Ing>* fileReader=new UpdaterReadBfmFile<Ing>(inputFilename,myIngredients,UpdaterReadBfmFile<Ing>::READ_LAST_CONFIG_FAST);
			fileReader->initialize();
			taskManager.addUpdater(fileReader,0);
			if(debyeVerletSkin>1.0)
			{
				myIngredients.setUseVerletList(true);
				myIngredients.setVerletSkin(debyeVerletSkin);
				myIngredients.synchronize(myIngredients);
			}
			
		}
		
		uint64_t shortTermInterval=100;
		
		
		//this is run only once at the beginning for equilibration.
		taskManager.addUpdater(new UpdaterSynchronize<Ing>(myIngredients),0);
		taskManager.addUpdater(new UpdaterSimpleSimulator<Ing,MoveLocalSc>(myIngredients,equilibration_steps),0);
		taskManager.addUpdater(new UpdaterBasicSimulator<Ing,MoveLocalSc>(myIngredients,shortTermInterval,upper_simulation_limit),1);
		/*add system consistency checker*/
		taskManager.addAnalyzer(latticeCheck,100*save_interval/shortTermInterval);
		taskManager.addAnalyzer(new UnmovedMonomersAnalyzer<Ing>(myIngredients),100*save_interval/shortTermInterval);
		if(cmd.getOption("--solvent_permeability")) {
			std::vector<int32_t> typeVector;typeVector.push_back(MonomerTypesCPU::SOLVENT);
			double limit;cmd.getOption("--solvent_permeability",limit,0);
			taskManager.addAnalyzer(new AnalyzerTranslocationProfile<Ing>(myIngredients,typeVector,-limit,limit),1);
		}
		if(cmd.getOption("--ion_permeability")) {
			std::vector<int32_t> typeVector;typeVector.push_back(MonomerTypesCPU::COUNTERION_A);
			double limit;cmd.getOption("--ion_permeability",limit,0);
			taskManager.addAnalyzer(new AnalyzerTranslocationProfile<Ing>(myIngredients,typeVector,-limit,limit),1);
			typeVector.clear();typeVector.push_back(MonomerTypesCPU::COUNTERION_B);
			taskManager.addAnalyzer(new AnalyzerTranslocationProfile<Ing>(myIngredients,typeVector,-limit,limit),1);
		}
		
		if(writeSolvent==true)
			taskManager.addAnalyzer(new AnalyzerWriteBfmFile<Ing>(outputFilename,myIngredients,AnalyzerWriteBfmFile<Ing>::NEWFILE),save_interval/shortTermInterval);
		else
		{
			std::string solventFilename=std::string("solvent_")+outputFilename;
			taskManager.addAnalyzer(new AnalyzerWriteBfmFile<Ing>(solventFilename,myIngredients,AnalyzerWriteBfmFile<Ing>::OVERWRITE),save_interval/shortTermInterval);
			taskManager.addAnalyzer(new AnalyzerWriteBfmFileSubGroup<Ing,notOfType<3> >(outputFilename,myIngredients,AnalyzerWriteBfmFile<Ing>::NEWFILE,notOfType<3>()),save_interval/shortTermInterval);
		}
		
		/*init and run the simulation*/
		taskManager.initialize();
		
		/*run simulation*/
		taskManager.run();
		taskManager.cleanup();
	}
	catch(std::runtime_error& e){std::cerr<<e.what()<<std::endl;}
	catch(std::exception& e){std::cerr<<e.what()<<std::endl;}
	
	return 0;
}