#ifndef UPDATER_RATAG_BILAYER_GPU_H
#define UPDATER_RATAG_BILAYER_GPU_H

#include <vector>
#include <stdint.h>

#include <LeMonADE/updater/AbstractUpdater.h>
#include <LeMonADE/utility/Vector3D.h>
#include <LeMonADE/utility/MonomerGroup.h>
#include <LeMonADE/utility/DepthIterator.h>
#include <LeMonADE/utility/DepthIteratorPredicates.h>


/*****************************************************************************
 * this updater retags the monomer from a gpu simulation so that they can be
 * analyzed like a cpu simulation
 *****************************************************************************/


template<class IngredientsType>
class UpdaterRetagBilayerGPU:public AbstractUpdater
{
public:
	typedef typename IngredientsType::molecules_type molecules_type;
	
	UpdaterRetagBilayerGPU(IngredientsType& ing);
	~UpdaterRetagBilayerGPU(){}
	
	//general updater functions
	bool execute(){return false;}
	void initialize();
	void cleanup(){};
	
private:
	
	IngredientsType& ingredients;
};

template<class IngredientsType>
UpdaterRetagBilayerGPU<IngredientsType>::UpdaterRetagBilayerGPU(IngredientsType& ing)
:ingredients(ing)
{
}


template<class IngredientsType>
void UpdaterRetagBilayerGPU<IngredientsType>::initialize()
{
	const int32_t GPU_TAIL_A=1;
	const int32_t GPU_HEAD_A=2;
	const int32_t GPU_SOLVENT=3;
	const int32_t GPU_TAIL_B=4;
	const int32_t GPU_HEAD_B=5;
	
	const int32_t CPU_TAIL=1;
	const int32_t CPU_HEAD=2;
	const int32_t CPU_SOLVENT=3;
	const int32_t CPU_CATION=6;
	const int32_t CPU_ANION=7;
	
	enum CPU_TAGS{TAIL=1,HEAD,SOLVENT,POLYMER_A,POLYMER_B,CATION,ANION};
	
	std::map<int32_t, uint32_t> nTypes;
	
	for(size_t n=0;n<ingredients.getMolecules().size();n++){
		int32_t tag=ingredients.getMolecules()[n].getAttributeTag();
		nTypes[tag]+=1;		
	}
	
	for(int32_t t=0;t<8;t++)
		std::cout<<"UpdaterRetagBilayerGPU initial: type "<<t<<" number "<<nTypes[t]<<std::endl;
	
	for(size_t n=0;n<ingredients.getMolecules().size();n++){
		int32_t tag=ingredients.getMolecules()[n].getAttributeTag();
		switch(tag){
			case GPU_TAIL_A : 
				ingredients.modifyMolecules()[n].setAttributeTag(CPU_TAIL);
				break;
			
			case GPU_TAIL_B : 
				ingredients.modifyMolecules()[n].setAttributeTag(CPU_TAIL);
				break;
			
			case GPU_HEAD_A : 
				if(ingredients.getMolecules()[n].getCharge()==0 || ingredients.getMolecules().getNumLinks(n)!=0)
					ingredients.modifyMolecules()[n].setAttributeTag(CPU_HEAD);
				else if(ingredients.getMolecules()[n].getCharge()==1 && ingredients.getMolecules().getNumLinks(n)==0)
					ingredients.modifyMolecules()[n].setAttributeTag(CPU_CATION);
				else if(ingredients.getMolecules()[n].getCharge()==-1 && ingredients.getMolecules().getNumLinks(n)==0)
					ingredients.modifyMolecules()[n].setAttributeTag(CPU_ANION);
				break;
				
			case GPU_HEAD_B : 
				ingredients.modifyMolecules()[n].setAttributeTag(CPU_HEAD);
				break;
				
			case GPU_SOLVENT : 
				ingredients.modifyMolecules()[n].setAttributeTag(CPU_SOLVENT);
				break;
			
			default:
				break;
			
		}
	}
	
	nTypes.clear();
	for(size_t n=0;n<ingredients.getMolecules().size();n++){
		int32_t tag=ingredients.getMolecules()[n].getAttributeTag();
		nTypes[tag]+=1;		
	}
	for(int32_t t=0;t<8;t++)
		std::cout<<"UpdaterRetagBilayerGPU summary: type "<<t<<" number "<<nTypes[t]<<std::endl;
}



#endif /*include guard*/