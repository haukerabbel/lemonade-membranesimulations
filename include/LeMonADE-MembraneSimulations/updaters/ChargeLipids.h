#ifndef CHARGE_LIPIDS_H_H
#define CHARGE_LIPIDS_H_H

#include<iostream>
#include<vector>
#include <set>
#include <stdint.h>
#include <cstdlib>

#include <LeMonADE/updater/AbstractUpdater.h>
#include <LeMonADE/utility/Vector3D.h>
#include <LeMonADE/utility/MonomerGroup.h>
#include <LeMonADE/utility/DepthIterator.h>
#include <LeMonADE/utility/DepthIteratorPredicates.h>

#include <LeMonADE-Extensions/utilities/IteratorPredicates.h>



/*****************************************************************************
 *this updater charges a percentage of lipids to be anionic or zwitterionic
 *the set of lipid molecules to choose from is given to the updater as an 
 *argument to the constructor. for zwitterionic lipids the topmost monomer is
 *charges positive, the bottom monomer of the head is charged negative. for 
 *anionic lipids, only the bottom monomer is charged negative.
 *****************************************************************************/


template<class IngredientsType>
class ChargeLipids:public AbstractUpdater
{
public:
	typedef typename IngredientsType::molecules_type molecules_type;
	
	ChargeLipids(IngredientsType& ing,uint32_t nAnionicLip, uint32_t nZwitterionicLip);
	~ChargeLipids(){}
	
	//general updater functions
	bool execute(){return false;}
	void initialize();
	void cleanup(){};
	
	//get and set the attribute tags used for lipid head and tail
	void setHeadTag(int32_t tag){headTag1=tag;}
	void setTailTag(int32_t tag){tailTag1=tag;}
	
	void setNAnionic(uint32_t n){nAnionic=n;}
	void setNZwitterionic(uint32_t n){nZwitterionic=n;}
	
private:
	
	IngredientsType& ingredients;
	int32_t headTag1,tailTag1;
	uint32_t nAnionic,nZwitterionic;
	
	void findLipidHeads(std::vector<MonomerGroup<typename IngredientsType::molecules_type> > & lipidHeads,const std::vector<MonomerGroup<typename IngredientsType::molecules_type> > & lipids);
	void chargeLipidHeads(const std::vector<MonomerGroup<typename IngredientsType::molecules_type> > & lipidHeads,uint32_t anionic,uint32_t zwitterionic);

};

template<class IngredientsType>
ChargeLipids<IngredientsType>::ChargeLipids(IngredientsType& ing,uint32_t nAnionicLip, uint32_t nZwitterionicLip)
:ingredients(ing)
,headTag1(2)
,tailTag1(1)
,nAnionic(nAnionicLip)
,nZwitterionic(nZwitterionicLip)
{

}

template<class IngredientsType>
void ChargeLipids<IngredientsType>::initialize()
{
	/*add charges to lipids, same amount to upper and lower leaflet*/
	std::vector<MonomerGroup<typename IngredientsType::molecules_type> > upperLeaflet;
	std::vector<MonomerGroup<typename IngredientsType::molecules_type> > lowerLeaflet;
	
	int32_t box_z=ingredients.getBoxZ();
	
	fill_connected_groups(ingredients.getMolecules(),upperLeaflet,MonomerGroup<typename IngredientsType::molecules_type>(ingredients.getMolecules()),partOfUpperLeaflet(headTag1,tailTag1,box_z/2));
	fill_connected_groups(ingredients.getMolecules(),lowerLeaflet,MonomerGroup<typename IngredientsType::molecules_type>(ingredients.getMolecules()),partOfLowerLeaflet(headTag1,tailTag1,box_z/2));	
	
	std::vector<MonomerGroup<typename IngredientsType::molecules_type> > headsUpperLeaflet;
	std::vector<MonomerGroup<typename IngredientsType::molecules_type> > headsLowerLeaflet;
	
	findLipidHeads(headsUpperLeaflet,upperLeaflet);
	findLipidHeads(headsLowerLeaflet,lowerLeaflet);
	
	if( (nAnionic+nZwitterionic) > (upperLeaflet.size()+lowerLeaflet.size()) )
	{
		std::stringstream errormessage;
		errormessage<<"ChargeLipids::execute(): given number of anionic "
		            <<"and zwitterionic lipids is more than total amount of lipids\n"
			    <<"anionic "<<nAnionic<<" zwitterionic "<<nZwitterionic<<" lipids total "<<upperLeaflet.size() + lowerLeaflet.size()<<std::endl;
		throw std::runtime_error(errormessage.str());
	}
	
	
	chargeLipidHeads(headsUpperLeaflet,nAnionic/2,nZwitterionic/2);
	chargeLipidHeads(headsLowerLeaflet,nAnionic-nAnionic/2,nZwitterionic-nZwitterionic/2);
	
}

template<class IngredientsType>
void ChargeLipids<IngredientsType>::findLipidHeads(std::vector<MonomerGroup<typename IngredientsType::molecules_type> > & lipidHeads,const std::vector<MonomerGroup<typename IngredientsType::molecules_type> > & lipids)
{
	//find the lipid heads
	for(size_t n=0;n<lipids.size();n++)
	{
		lipidHeads.push_back(MonomerGroup<molecules_type>(ingredients.modifyMolecules()));
		for(size_t m=0;m<lipids[n].size();m++)
		{
			if(lipids[n][m].getAttributeTag()==headTag1 )
			{
				lipidHeads[n].push_back(lipids[n].trueIndex(m));
			}	
		}
	}
}

template<class IngredientsType>
void ChargeLipids<IngredientsType>::chargeLipidHeads(const std::vector<MonomerGroup<typename IngredientsType::molecules_type> > & lipidHeads,uint32_t anionic,uint32_t zwitterionic)
{
	molecules_type& molecules=ingredients.modifyMolecules();
	
	
	//make a set of lipid indices to keep track of what has been charged
	std::vector<int32_t> unusedLipids;
	for(int32_t n=0;n<lipidHeads.size();n++)
	{
		unusedLipids.push_back(n);
	}
	//make anionic lipids
	for(uint32_t n=0;n<anionic;n++)
	{
	 	int randomLipid=std::rand()%(unusedLipids.size());
		int32_t lipidIndex=unusedLipids[randomLipid];
		molecules[lipidHeads[lipidIndex].trueIndex(0)].setCharge(-1);
		//delete used element
		unusedLipids[randomLipid]=unusedLipids[unusedLipids.size()-1];
		unusedLipids.pop_back();
		
	}
	//make zwitterionic lipids
	for(int32_t n=0;n<zwitterionic;n++)
	{
	 	int randomLipid=std::rand()%(unusedLipids.size());
		int32_t lipidIndex=unusedLipids[randomLipid];
		molecules[lipidHeads[lipidIndex].trueIndex(0)].setCharge(-1);
		molecules[lipidHeads[lipidIndex].trueIndex(lipidHeads[lipidIndex].size()-1)].setCharge(1);
		//delete used element
		unusedLipids[randomLipid]=unusedLipids[unusedLipids.size()-1];
		unusedLipids.pop_back();
		
	}
	std::cout<<"ChargeLipids: made "<<anionic<<" anionic and "<<zwitterionic<<" zwitterionic lipids\n";
}
#endif /*CHARGE_LIPIDS_H*/