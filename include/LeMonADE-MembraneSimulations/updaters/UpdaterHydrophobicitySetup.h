#ifndef UPDATER_HYDROPHOBICITY_SETUP_H
#define UPDATER_HYDROPHOBICITY_SETUP_H

#include <vector>
#include <stdint.h>
#include <cmath>

#include <LeMonADE/updater/AbstractUpdater.h>
#include <LeMonADE/utility/Vector3D.h>
#include <LeMonADE/utility/MonomerGroup.h>
#include <LeMonADE/utility/DepthIterator.h>
#include <LeMonADE/utility/DepthIteratorPredicates.h>

#include<LeMonADE-MembraneSimulations/utilities/MonomerTypes.h>
/*****************************************************************************
 * prepares the nn interaction constants based on hydrophobicity and interaction
 * base constant
 *****************************************************************************/


template<class IngredientsType>
class UpdaterHydrophobicitySetup:public AbstractUpdater
{
public:
	
	UpdaterHydrophobicitySetup(IngredientsType& ing,std::vector<double> hydrophob, double e0=0.8);
	~UpdaterHydrophobicitySetup(){}
	
	//general updater functions
	bool execute(){return false;}
	void initialize();
	void cleanup(){};
	
private:
	double epsilon0;
	std::vector<double> hydrophobicity;
	IngredientsType& ingredients;
	
};

template<class IngredientsType>
UpdaterHydrophobicitySetup<IngredientsType>::UpdaterHydrophobicitySetup(IngredientsType& ing,std::vector<double> hydrophob,double e0)
:ingredients(ing)
,hydrophobicity(hydrophob)
,epsilon0(e0)
{
}


template<class IngredientsType>
void UpdaterHydrophobicitySetup<IngredientsType>::initialize()
{
	double epsilon_TS=hydrophobicity[MonomerTypesCPU::LIPID_TAIL-1]*epsilon0;
	double epsilon_HS=hydrophobicity[MonomerTypesCPU::LIPID_HEAD-1]*epsilon0;
	double epsilon_AS=hydrophobicity[MonomerTypesCPU::POLYMER_A-1]*epsilon0;
	double epsilon_BS=hydrophobicity[MonomerTypesCPU::POLYMER_B-1]*epsilon0;
	double epsilon_CaS=hydrophobicity[MonomerTypesCPU::COUNTERION_A-1]*epsilon0;
	double epsilon_CbS=hydrophobicity[MonomerTypesCPU::COUNTERION_B-1]*epsilon0;
	double epsilon_TcS=hydrophobicity[MonomerTypesCPU::TYPE_C-1]*epsilon0;
	//interactions with lipid head
	double epsilon_TH=std::fabs(epsilon_TS-epsilon_HS);
	double epsilon_AH=std::fabs(epsilon_AS-epsilon_HS);
	double epsilon_BH=std::fabs(epsilon_BS-epsilon_HS);
	double epsilon_CaH=std::fabs(epsilon_CaS-epsilon_HS);
	double epsilon_CbH=std::fabs(epsilon_CbS-epsilon_HS);
	double epsilon_TcH=std::fabs(epsilon_TcS-epsilon_HS);
	//interactions with lipid tail
	double epsilon_AT=std::fabs(epsilon_AS-epsilon_TS);
	double epsilon_BT=std::fabs(epsilon_BS-epsilon_TS);
	double epsilon_CaT=std::fabs(epsilon_CaS-epsilon_TS);
	double epsilon_CbT=std::fabs(epsilon_CbS-epsilon_TS);
	double epsilon_TcT=std::fabs(epsilon_TcS-epsilon_TS);
	//interactions with polymerA
	double epsilon_BA=std::fabs(epsilon_BS-epsilon_AS);
	double epsilon_CaA=std::fabs(epsilon_CaS-epsilon_AS);
	double epsilon_CbA=std::fabs(epsilon_CbS-epsilon_AS);
	double epsilon_TcA=std::fabs(epsilon_TcS-epsilon_AS);
	//interactons with polymerB
	double epsilon_CaB=std::fabs(epsilon_CaS-epsilon_BS);
	double epsilon_CbB=std::fabs(epsilon_CbS-epsilon_BS);
	double epsilon_TcB=std::fabs(epsilon_TcS-epsilon_BS);
	//interacton with counterionA
	double epsilon_CbCa=std::fabs(epsilon_CbS-epsilon_CaS);
	double epsilon_TcCa=std::fabs(epsilon_TcS-epsilon_CaS);
	//interaction with counterionB
	double epsilon_TcCb=std::fabs(epsilon_TcS-epsilon_CbS);
	
	/*no self interaction*/
	ingredients.setNNInteraction(MonomerTypesCPU::LIPID_TAIL,MonomerTypesCPU::LIPID_TAIL,0.0);
	ingredients.setNNInteraction(MonomerTypesCPU::LIPID_HEAD,MonomerTypesCPU::LIPID_HEAD,0.0);
	ingredients.setNNInteraction(MonomerTypesCPU::SOLVENT,MonomerTypesCPU::SOLVENT,0.0);
	ingredients.setNNInteraction(MonomerTypesCPU::POLYMER_A,MonomerTypesCPU::POLYMER_A,0.0);
	ingredients.setNNInteraction(MonomerTypesCPU::POLYMER_B,MonomerTypesCPU::POLYMER_B,0.0);
	ingredients.setNNInteraction(MonomerTypesCPU::COUNTERION_A,MonomerTypesCPU::COUNTERION_A,0.0);
	ingredients.setNNInteraction(MonomerTypesCPU::COUNTERION_B,MonomerTypesCPU::COUNTERION_B,0.0);
	ingredients.setNNInteraction(MonomerTypesCPU::TYPE_C,MonomerTypesCPU::TYPE_C,0.0);
	
	/*now set the above defined interaction constants*/
	ingredients.setNNInteraction(MonomerTypesCPU::LIPID_TAIL,MonomerTypesCPU::SOLVENT,epsilon_TS);
	ingredients.setNNInteraction(MonomerTypesCPU::LIPID_HEAD,MonomerTypesCPU::SOLVENT,epsilon_HS);
	ingredients.setNNInteraction(MonomerTypesCPU::POLYMER_A,MonomerTypesCPU::SOLVENT,epsilon_AS);
	ingredients.setNNInteraction(MonomerTypesCPU::POLYMER_B,MonomerTypesCPU::SOLVENT,epsilon_BS);
	ingredients.setNNInteraction(MonomerTypesCPU::COUNTERION_A,MonomerTypesCPU::SOLVENT,epsilon_CaS);
	ingredients.setNNInteraction(MonomerTypesCPU::COUNTERION_B,MonomerTypesCPU::SOLVENT,epsilon_CbS);
	ingredients.setNNInteraction(MonomerTypesCPU::TYPE_C,MonomerTypesCPU::SOLVENT,epsilon_TcS);
	
	ingredients.setNNInteraction(MonomerTypesCPU::LIPID_TAIL,MonomerTypesCPU::LIPID_HEAD,epsilon_TH);
	ingredients.setNNInteraction(MonomerTypesCPU::POLYMER_A,MonomerTypesCPU::LIPID_HEAD,epsilon_AH);
	ingredients.setNNInteraction(MonomerTypesCPU::POLYMER_B,MonomerTypesCPU::LIPID_HEAD,epsilon_BH);
	ingredients.setNNInteraction(MonomerTypesCPU::COUNTERION_A,MonomerTypesCPU::LIPID_HEAD,epsilon_CaH);
	ingredients.setNNInteraction(MonomerTypesCPU::COUNTERION_B,MonomerTypesCPU::LIPID_HEAD,epsilon_CbH);
	ingredients.setNNInteraction(MonomerTypesCPU::TYPE_C,MonomerTypesCPU::LIPID_HEAD,epsilon_TcH);
	
	ingredients.setNNInteraction(MonomerTypesCPU::POLYMER_A,MonomerTypesCPU::LIPID_TAIL,epsilon_AT);
	ingredients.setNNInteraction(MonomerTypesCPU::POLYMER_B,MonomerTypesCPU::LIPID_TAIL,epsilon_BT);
	ingredients.setNNInteraction(MonomerTypesCPU::COUNTERION_A,MonomerTypesCPU::LIPID_TAIL,epsilon_CaT);
	ingredients.setNNInteraction(MonomerTypesCPU::COUNTERION_B,MonomerTypesCPU::LIPID_TAIL,epsilon_CbT);
	ingredients.setNNInteraction(MonomerTypesCPU::TYPE_C,MonomerTypesCPU::LIPID_TAIL,epsilon_TcT);
	
	ingredients.setNNInteraction(MonomerTypesCPU::POLYMER_B,MonomerTypesCPU::POLYMER_A,epsilon_BA);
	ingredients.setNNInteraction(MonomerTypesCPU::COUNTERION_A,MonomerTypesCPU::POLYMER_A,epsilon_CaA);
	ingredients.setNNInteraction(MonomerTypesCPU::COUNTERION_B,MonomerTypesCPU::POLYMER_A,epsilon_CbA);
	ingredients.setNNInteraction(MonomerTypesCPU::TYPE_C,MonomerTypesCPU::POLYMER_A,epsilon_TcA);
	
	ingredients.setNNInteraction(MonomerTypesCPU::COUNTERION_A,MonomerTypesCPU::POLYMER_B,epsilon_CaB);
	ingredients.setNNInteraction(MonomerTypesCPU::COUNTERION_B,MonomerTypesCPU::POLYMER_B,epsilon_CbB);
	ingredients.setNNInteraction(MonomerTypesCPU::TYPE_C,MonomerTypesCPU::POLYMER_B,epsilon_TcB);
	
	ingredients.setNNInteraction(MonomerTypesCPU::COUNTERION_B,MonomerTypesCPU::COUNTERION_A,epsilon_CbCa);
	ingredients.setNNInteraction(MonomerTypesCPU::TYPE_C,MonomerTypesCPU::COUNTERION_A,epsilon_TcCa);
	
	ingredients.setNNInteraction(MonomerTypesCPU::TYPE_C,MonomerTypesCPU::COUNTERION_B,epsilon_TcCb);
	
	ingredients.synchronize();
	
}



#endif /*include guard*/