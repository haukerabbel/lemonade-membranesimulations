#ifndef UPDATER_CREATE_CHARGED_RCP_H
#define UPDATER_CREATE_CHARGED_RCP_H

#include<cmath>
#include<vector>

#include <LeMonADE/utility/RandomNumberGenerators.h>
#include <LeMonADE/updater/UpdaterAbstractCreate.h>

template<class IngredientsType>
class UpdaterCreateChargedRCP:public UpdaterAbstractCreate<IngredientsType>
{
public:
	UpdaterCreateChargedRCP(IngredientsType& ing,
				 int32_t typeA,
			  int32_t typeB,
			  uint32_t nmonosA,
			  uint32_t nmonosB,
			  int32_t chargeA,
			  int32_t chargeB,
			  uint32_t npolymers=1);
	
	~UpdaterCreateChargedRCP(){}
	
	virtual void initialize();
	virtual bool execute(){return false;}
	virtual void cleanup(){};
	
	
private:
	IngredientsType& ingredients;
	int32_t monoTypeA,monoTypeB,monoChargeA,monoChargeB;
	uint32_t nMonosA,nMonosB,nPolymers;
	
	using UpdaterAbstractCreate<IngredientsType>::addSingleMonomer;
	using UpdaterAbstractCreate<IngredientsType>::addMonomerToParent;
	
};



template<class IngredientsType>
UpdaterCreateChargedRCP<IngredientsType>::UpdaterCreateChargedRCP(IngredientsType& ing,
								    int32_t typeA,
								    int32_t typeB,
								    uint32_t nmonosA,
								    uint32_t nmonosB,
								    int32_t chargeA,
								    int32_t chargeB,
								    uint32_t npolymers)
:UpdaterAbstractCreate<IngredientsType>(ing)
,ingredients(ing)
,monoTypeA(typeA)
,monoTypeB(typeB)
,nMonosA(nmonosA)
,nMonosB(nmonosB)
,monoChargeA(chargeA)
,monoChargeB(chargeB)
,nPolymers(npolymers)
{
}

template<class IngredientsType>
void UpdaterCreateChargedRCP<IngredientsType>::initialize()
{
	
	RandomNumberGenerators randomNumbers;
	
	std::vector<size_t> chainEnds(nPolymers,0);
	std::vector<std::vector<int32_t> > monoTypes,monoTypesInit;
	
	monoTypes.resize(nPolymers);
	monoTypesInit.resize(nPolymers);
	
	for(size_t p=0;p<monoTypesInit.size();p++){
		
		for(size_t n=0;n<nMonosA;n++){
			monoTypesInit[p].push_back(monoTypeA);
		}
		for(size_t n=0;n<nMonosB;n++){
			monoTypesInit[p].push_back(monoTypeB);
		}
	}
	
	for(size_t p=0;p<monoTypesInit.size();p++){
		
		for(size_t n=0;n<nMonosA+nMonosB;n++){
			size_t idx=randomNumbers.r250_rand32()%monoTypesInit[p].size();
			monoTypes[p].push_back(monoTypesInit[p][idx]);
			monoTypesInit[p][idx]=*monoTypesInit[p].rbegin();
			monoTypesInit[p].pop_back();
		}
	}
	
	
	//add initial monomers
	for(uint32_t n=0;n<nPolymers;n++){
		//create initial monomer
		if(addSingleMonomer(monoTypes[n][0])){
			chainEnds[n]=ingredients.getMolecules().size()-1;
			int32_t Q=(monoTypes[n][0]==monoTypeA ? monoChargeA : monoChargeB);
			ingredients.modifyMolecules()[chainEnds[n]].setCharge(Q);
		}
		else{
			std::stringstream errormessage;
			errormessage<<"UpdaterCreateChargedRCP::error adding initial monomer for chain no "<<n;
			throw std::runtime_error(errormessage.str());
		}
		
		
		for(uint32_t l=1;l<monoTypes[n].size();l++){
			
			//create initial monomer
			if(addMonomerToParent(chainEnds[n],monoTypes[n][l])){
				chainEnds[n]=ingredients.getMolecules().size()-1;
				int32_t Q=(monoTypes[n][l]==monoTypeA ? monoChargeA : monoChargeB);
				ingredients.modifyMolecules()[chainEnds[n]].setCharge(Q);
			}
			else{
				std::stringstream errormessage;
				errormessage<<"UpdaterCreateChargedRCP::error adding monomer "<<l<<"to chain no "<<n;
				throw std::runtime_error(errormessage.str());
			}
			
		}
	}
	ingredients.synchronize();
}

#endif /*include guard*/