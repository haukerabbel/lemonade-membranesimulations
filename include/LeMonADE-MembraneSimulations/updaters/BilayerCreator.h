#ifndef BILAYER_CREATOR_H
#define BILAYER_CREATOR_H

#include <stdint.h>
#include <cmath>
#include <cstdlib>

#include <LeMonADE/updater/AbstractUpdater.h>
#include <LeMonADE/utility/Vector3D.h>
#include <LeMonADE/updater/moves/MoveAddMonomerSc.h>



/*****************************************************************************
 *this updater creates a lipid bilayer out of a number of nLipids lipids.
 *this number is given to the constructor of the updater as second argument.
 *required feature: FeatureAttributes.
 *it works with standard bfm only (simple cubic lattice), because it
 *uses MoveAddScMonomer to insert lipid monomers into the system.
 *****************************************************************************/


template<class IngredientsType>
class BilayerCreator:public AbstractUpdater
{
public:
	BilayerCreator(IngredientsType& ingredients,uint32_t nLipids);
	~BilayerCreator(){}
	
	typedef typename IngredientsType::molecules_type molecules_type;
	
	//general updater functions
	bool execute(){return false;}
	void initialize();
	void cleanup(){};
	
	//get and set the attribute tags used for lipid head and tail
	int32_t getHeadTag() const{return headTag_;}
	int32_t getTailTag() const{return tailTag_;}
	void setHeadTag(int32_t tag){headTag_=tag;}
	void setTailTag(int32_t tag){tailTag_=tag;}
	
	//change the type of lipid that is used for both leaflets
	void setLipidTemplateUp(molecules_type t){lipidTemplateUp=t;}
	void setLipidTemplateDown(molecules_type t){lipidTemplateDown=t;}
	
	void setNLipidsUpperLeaflet(uint32_t n){nLipidsUpperLeaflet_=n;}
	void setNLipidsLowerLeaflet(uint32_t n){nLipidsLowerLeaflet_=n;}
private:
	//finds possible positions for inserting lipids
	void findFreeLipidSitesTopLeaflet();
	void findFreeLipidSitesBottomLeaflet();
	int32_t findMinLipidSpacingTop();
	int32_t findMinLipidSpacingBottom();
	//adds one lipid above position x,y,z to the system
	bool addLipidUp(int32_t x,int32_t y,int32_t z);
	//adds one lipid below position x,y,z to the system
	bool addLipidDown(int32_t x,int32_t y,int32_t z);
	
	IngredientsType& ingredients_;
	uint32_t nLipids_,nLipidsUpperLeaflet_,nLipidsLowerLeaflet_;
	int32_t headTag_,tailTag_;
	
	//contain positions for lipid in up and down directions
	molecules_type  lipidTemplateUp;
	molecules_type  lipidTemplateDown;
	//contains free sites for lipid insertion in z=boxZ/2 plane
	std::vector<VectorInt3> freeSitesTop;
	std::vector<VectorInt3> freeSitesBottom;

};


////////////////////////////////////////////////////////////////////////////////
//constuctor: sets up lipid templates
////////////////////////////////////////////////////////////////////////////////
template<class IngredientsType>
BilayerCreator<IngredientsType>::BilayerCreator(IngredientsType& ingredients, uint32_t nLipids)
:ingredients_(ingredients)
,nLipids_(nLipids)
,nLipidsUpperLeaflet_(nLipids/2)
,nLipidsLowerLeaflet_(nLipids/2)
,headTag_(2)
,tailTag_(1)
{
	//at some point it would be nice to have the lipid template as argument
	//to the constructor. this way one could easily create different lipids.
	for(int32_t n=0;n<5;n++)
	{
		lipidTemplateUp.addMonomer(-1,0,1+2*n);
		lipidTemplateDown.addMonomer(0,-1,-1-2*n);
		lipidTemplateUp[lipidTemplateUp.size()-1].setAttributeTag(tailTag_);
		lipidTemplateDown[lipidTemplateDown.size()-1].setAttributeTag(tailTag_);
		if(n>0)
		{
			lipidTemplateUp.connect(n,n-1);
			lipidTemplateDown.connect(n,n-1);
		}
	}
	for(int32_t n=5;n<8;n++)
	{
		lipidTemplateUp.addMonomer(0,0,1+2*n);
		lipidTemplateDown.addMonomer(0,0,-1-2*n);
		lipidTemplateUp[lipidTemplateUp.size()-1].setAttributeTag(headTag_);
		lipidTemplateDown[lipidTemplateDown.size()-1].setAttributeTag(headTag_);
		lipidTemplateUp.connect(n,n-1);
		lipidTemplateDown.connect(n,n-1);
	}
	for(int32_t n=8;n<13;n++)
	{
		int32_t m=n-8;
		lipidTemplateUp.addMonomer(+1,0,1+2*m);
		lipidTemplateDown.addMonomer(0,+1,-1-2*m);
		lipidTemplateUp[lipidTemplateUp.size()-1].setAttributeTag(tailTag_);
		lipidTemplateDown[lipidTemplateDown.size()-1].setAttributeTag(tailTag_);
		if(m>0)
		{
			lipidTemplateUp.connect(n,n-1);
			lipidTemplateDown.connect(n,n-1);
		}
	}
	//connect second tail to head
	lipidTemplateUp.connect(5,12);
	lipidTemplateDown.connect(5,12);
	//if nLipids was an odd number, make the upper leaflet stronger by one
	if(nLipidsUpperLeaflet_+nLipidsLowerLeaflet_<nLipids) nLipidsUpperLeaflet_+=1;
	
}

////////////////////////////////////////////////////////////////////////////////
//initialize: creates the bilayer
////////////////////////////////////////////////////////////////////////////////
template<class IngredientsType>
void BilayerCreator<IngredientsType>::initialize()
{

  //fill the vector freeSites for lipid insertion
  findFreeLipidSitesTopLeaflet();
  findFreeLipidSitesBottomLeaflet();
  
  bool isEnoughSpaceTop=(nLipidsUpperLeaflet_<=freeSitesTop.size());
  bool isEnoughSpaceBottom=(nLipidsLowerLeaflet_<=freeSitesBottom.size());
  if(isEnoughSpaceBottom && isEnoughSpaceTop)
  {
      //fill upper leaflet	  
      for(uint32_t n=0;n<nLipidsUpperLeaflet_;n++)
      {
          //pick a random free site
          size_t randomSite=rand()%freeSitesTop.size();
          //insert lipid at this position
          int32_t x=freeSitesTop[randomSite].getX();
          int32_t y=freeSitesTop[randomSite].getY();
          addLipidUp(x,y,ingredients_.getBoxZ()/2);
          
          //delete used site from the vector of free sites
          std::vector<VectorInt3>::iterator it=freeSitesTop.begin();
          for(uint32_t k=0;k<randomSite;++k)
          {
              ++it;
          }
          freeSitesTop.erase(it);
      }
      //fill lower leaflet	  
      for(uint32_t n=0;n<nLipidsLowerLeaflet_;n++)
      {
          //pick a random free site
          size_t randomSite=rand()%freeSitesBottom.size();
          //insert lipid at this position
          int32_t x=freeSitesBottom[randomSite].getX();
          int32_t y=freeSitesBottom[randomSite].getY();
          addLipidDown(x,y,ingredients_.getBoxZ()/2);
          
          //delete used site from the vector of free sites
          std::vector<VectorInt3>::iterator it=freeSitesBottom.begin();
          for(uint32_t k=0;k<randomSite;++k)
          {
              ++it;
          }
          freeSitesBottom.erase(it);
      }
  }
  else
    {
      std::string errormessage("BilayerCreator::execute(): more lipids requested then space available.\n");
      throw std::runtime_error(errormessage);
    }


}

////////////////////////////////////////////////////////////////////////////////
//addLipidUp: adds a lipid according to lipidTemplateUp to position x,y,z
////////////////////////////////////////////////////////////////////////////////
template<class IngredientsType>
bool BilayerCreator<IngredientsType>::addLipidUp(int32_t x,int32_t y,int32_t z)
{

    MoveAddMonomerSc trialMove;

    molecules_type& molecules=ingredients_.modifyMolecules();

    //add the lipids by setting the position of the trial move
    //and applying the move to the system using trialMove.apply(...)
    size_t currentSize=molecules.size();
    for(size_t n=0;n<lipidTemplateUp.size();n++)
    {
        trialMove.setPosition(lipidTemplateUp[n]+VectorInt3(x,y,z));
        trialMove.setTag(lipidTemplateUp[n].getAttributeTag());
        //add the monomer to this position. the position is not checked again here
        //it must be checked before
        trialMove.apply(ingredients_);
        

    }

    //connect the monomers
    for(size_t n=0;n<lipidTemplateUp.size();n++)
    {
        for(size_t m=0;m<lipidTemplateUp.getNumLinks(n);m++)
	{
		if(!molecules.areConnected(currentSize+n,currentSize+lipidTemplateUp.getNeighborIdx(n,m)))
			molecules.connect(currentSize+n,currentSize+lipidTemplateUp.getNeighborIdx(n,m));
	}
        
    }
    return true;

}



////////////////////////////////////////////////////////////////////////////////
//addLipidDown: adds a lipid according to lipidTemplateDown to position x,y,z
////////////////////////////////////////////////////////////////////////////////
template<class IngredientsType>
bool BilayerCreator<IngredientsType>::addLipidDown(int32_t x,int32_t y,int32_t z)
{
    MoveAddMonomerSc trialMove;

    molecules_type& molecules=ingredients_.modifyMolecules();

    //add the lipids by setting the position of the trial move
    //and applying the move to the system using trialMove.apply(...)
    size_t currentSize=molecules.size();
    for(size_t n=0;n<lipidTemplateDown.size();n++)
    {
        trialMove.setPosition(lipidTemplateDown[n]+VectorInt3(x,y,z));
        trialMove.setTag(lipidTemplateDown[n].getAttributeTag());
        //add the monomer to this position. the position is not checked again here
        //it must be checked before
        trialMove.apply(ingredients_);
        

    }

    //connect the monomers
    for(size_t n=0;n<lipidTemplateDown.size();n++)
    {
        for(size_t m=0;m<lipidTemplateDown.getNumLinks(n);m++)
	{
		if(!molecules.areConnected(currentSize+n,currentSize+lipidTemplateDown.getNeighborIdx(n,m)))
			molecules.connect(currentSize+n,currentSize+lipidTemplateDown.getNeighborIdx(n,m));
	}
        
    }
    return true;
}


////////////////////////////////////////////////////////////////////////////////
//findFreeLipidSites: looks for possible positions in the plane z=boxZ/2, where
//a pair of lipids can be inserted (one pointing upwards, one downwards and
//90 deg rotated around long axix). these positions are saved in the vector
//freeSites. the positions are looked for on a grid with spacing of four lattice
//units.
////////////////////////////////////////////////////////////////////////////////
template<class IngredientsType>
void BilayerCreator<IngredientsType>::findFreeLipidSitesTopLeaflet()
{
    //first empty the freeSites vector
    freeSitesTop.clear();
    //now look for free sites using MoveAddScMonomer
    MoveAddMonomerSc trialMove;

    int32_t minLipidSpacing=findMinLipidSpacingTop();

    //go through the grid of x,y positions and use the trial move
    //to check if all monomers of a lipid could be inserted there
    for(int32_t x=1; x<ingredients_.getBoxX();x+=minLipidSpacing)
    {
        for(int32_t y=1;y<ingredients_.getBoxY();y+=minLipidSpacing)
        {
            VectorInt3 centerPosition(x,y,ingredients_.getBoxZ()/2);
            bool accepted=true;

            for(size_t n=0;n<lipidTemplateUp.size();n++)
            {
                trialMove.setTag(lipidTemplateUp[n].getAttributeTag());
                //check the positions here, using the check function of the move
                trialMove.resetProbability();
                trialMove.setPosition(centerPosition+lipidTemplateUp[n]);
                accepted=accepted && trialMove.check(ingredients_);

            }

            if(accepted==true)
                freeSitesTop.push_back(centerPosition);

        }
    }

    std::cout<<"BilayerCreator::findFreeLipidSitesTopLeaflet(): found "<<freeSitesTop.size()<<" possible positions\n";
}

template<class IngredientsType>
void BilayerCreator<IngredientsType>::findFreeLipidSitesBottomLeaflet()
{
    //first empty the freeSites vector
    freeSitesBottom.clear();
    //now look for free sites using MoveAddScMonomer
    MoveAddMonomerSc trialMove;

    int32_t minLipidSpacing=findMinLipidSpacingBottom();

    //go through the grid of x,y positions and use the trial move
    //to check if all monomers of a lipid could be inserted there
    for(int32_t x=1; x<ingredients_.getBoxX();x+=minLipidSpacing)
    {
        for(int32_t y=1;y<ingredients_.getBoxY();y+=minLipidSpacing)
        {
            VectorInt3 centerPosition(x,y,ingredients_.getBoxZ()/2);
            bool accepted=true;

            for(size_t n=0;n<lipidTemplateDown.size();n++)
            {
                trialMove.setTag(lipidTemplateDown[n].getAttributeTag());
                //check the positions here, using the check function of the move
                trialMove.resetProbability();
                trialMove.setPosition(centerPosition+lipidTemplateDown[n]);
                accepted=accepted && trialMove.check(ingredients_);
            }

            if(accepted==true)
                freeSitesBottom.push_back(centerPosition);

        }
    }

    std::cout<<"BilayerCreator::findFreeLipidSitesBottomLeaflet(): found "<<freeSitesBottom.size()<<" possible positions\n";
}

template<class IngredientsType>
int32_t BilayerCreator<IngredientsType>::findMinLipidSpacingTop()
{
	int32_t spacing,minX,maxX,minY,maxY;
	minX=lipidTemplateUp[0].getX();
	maxX=lipidTemplateUp[0].getX();
	minY=lipidTemplateUp[0].getY();
	maxY=lipidTemplateUp[0].getY();
	//find smallest x and y coordinates, then largest, spacing is the largest difference
	for(size_t n=0;n<lipidTemplateUp.size();n++)
	{
		if(lipidTemplateUp[n].getX()<minX) minX=lipidTemplateUp[n].getX();
		if(lipidTemplateUp[n].getX()>maxX) maxX=lipidTemplateUp[n].getX();
		if(lipidTemplateUp[n].getY()<minX) minY=lipidTemplateUp[n].getY();
		if(lipidTemplateUp[n].getY()>maxX) maxY=lipidTemplateUp[n].getY();
	}
	spacing=std::max(abs(maxX-minX),abs(maxY-minY));
	std::cout<<"min lipid spacig top "<<spacing<<std::endl;
	return spacing+2;
}

template<class IngredientsType>
int32_t BilayerCreator<IngredientsType>::findMinLipidSpacingBottom()
{
	int32_t spacing,minX,maxX,minY,maxY;
	minX=lipidTemplateDown[0].getX();
	maxX=lipidTemplateDown[0].getX();
	minY=lipidTemplateDown[0].getY();
	maxY=lipidTemplateDown[0].getY();
	//find smallest x and y coordinates, then largest, spacing is the largest difference
	for(size_t n=0;n<lipidTemplateDown.size();n++)
	{
		if(lipidTemplateDown[n].getX()<minX) minX=lipidTemplateDown[n].getX();
		if(lipidTemplateDown[n].getX()>maxX) maxX=lipidTemplateDown[n].getX();
		if(lipidTemplateDown[n].getY()<minX) minY=lipidTemplateDown[n].getY();
		if(lipidTemplateDown[n].getY()>maxX) maxY=lipidTemplateDown[n].getY();
	}
	spacing=std::max(abs(maxX-minX),abs(maxY-minY));
	std::cout<<"min lipid spacig bottom "<<spacing<<std::endl;
	return spacing+2;
}





// template<class IngredientsType>
// class BilayerCreator:public AbstractUpdater
// {
// public:
// 	BilayerCreator(IngredientsType& ingredients,uint32_t nLipids);
// 	~BilayerCreator(){}
// 	
// 	//general updater functions
// 	bool execute();
// 	void initialize(){};
// 	void cleanup(){};
// 	
// 	//get and set the attribute tags used for lipid head and tail
// 	int32_t getHeadTag() const{return headTag_;}
// 	int32_t getTailTag() const{return tailTag_;}
// 	void setHeadTag(int32_t tag){headTag_=tag;}
// 	void setTailTag(int32_t tag){tailTag_=tag;}
// 	
// private:
// 	//finds possible positions for inserting pairs of lipids in the
// 	//plane z=boxZ/2. these positions are stored in the vector freeSites
// 	//(private member of this class)
// 	void findFreeLipidSites();
// 	//adds one lipid above position x,y,z to the system
// 	bool addLipidUp(int32_t x,int32_t y,int32_t z);
// 	//adds one lipid below position x,y,z to the system
// 	bool addLipidDown(int32_t x,int32_t y,int32_t z);
// 	
// 	IngredientsType& ingredients_;
// 	uint32_t nLipids_;
// 	int32_t headTag_,tailTag_;
// 	
// 	//contain positions for lipid in up and down directions
// 	std::vector<VectorInt3>  lipidTemplateUp;
// 	std::vector<VectorInt3>  lipidTemplateDown;
// 	//contains free sites for lipid insertion in z=boxZ/2 plane
// 	std::vector<VectorInt3> freeSites;
// 
// };
// 
// 
// ////////////////////////////////////////////////////////////////////////////////
// //constuctor: sets up lipid templates
// ////////////////////////////////////////////////////////////////////////////////
// template<class IngredientsType>
// BilayerCreator<IngredientsType>::BilayerCreator(IngredientsType& ingredients, uint32_t nLipids)
// 	:ingredients_(ingredients)
// 	,nLipids_(nLipids)
//     ,headTag_(2)
//     ,tailTag_(1)
// {
//     //at some point it would be nice to have the lipid template as argument
//     //to the constructor. this way one could easily create different lipids.
//     for(int32_t n=0;n<5;n++)
//     {
//         lipidTemplateUp.push_back(VectorInt3(-1,0,1+2*n));
//         lipidTemplateDown.push_back(VectorInt3(0,-1,-1-2*n));
//     }
//     for(int32_t n=5;n<8;n++)
//     {
//         lipidTemplateUp.push_back(VectorInt3(0,0,1+2*n));
//         lipidTemplateDown.push_back(VectorInt3(0,0,-1-2*n));
//     }
//     for(int32_t n=0;n<5;n++)
//     {
//         lipidTemplateUp.push_back(VectorInt3(+1,0,1+2*n));
//         lipidTemplateDown.push_back(VectorInt3(0,+1,-1-2*n));
//     }
// 
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// //execute: creates the bilayer
// ////////////////////////////////////////////////////////////////////////////////
// template<class IngredientsType>
// bool BilayerCreator<IngredientsType>::execute()
// {
// 
//   //fill the vector freeSites for lipid insertion
//   findFreeLipidSites();
//   //now choose nLipids/2 sites and insert two lipids at each of them
//   //(one using addLipidUp(x,y,z) and one using addLipidDown(x,y,z)
//   //do this only if there are enough free sites!
//   if(2*freeSites.size()>=nLipids_)
//   {
//       for(uint32_t n=0;n<nLipids_;n+=2)
//       {
//           //pick a random free site
//           size_t randomSite=rand()%freeSites.size();
//           //insert two lipids, one up, one down, at this position
//           int32_t x=freeSites[randomSite].getX();
//           int32_t y=freeSites[randomSite].getY();
//           addLipidUp(x,y,ingredients_.getBoxZ()/2);
//           addLipidDown(x,y,ingredients_.getBoxZ()/2);
//           //delete used site from the vector of free sites
//           std::vector<VectorInt3>::iterator it=freeSites.begin();
//           for(uint32_t k=0;k<randomSite;++k)
//           {
//               ++it;
//           }
//           freeSites.erase(it);
//       }
//   }
//   else
//     {
//       std::string errormessage("BilayerCreator::execute(): more lipids requested then space available.\n");
//       throw std::runtime_error(errormessage);
//     }
// 
// 
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// //addLipidUp: adds a lipid according to lipidTemplateUp to position x,y,z
// ////////////////////////////////////////////////////////////////////////////////
// template<class IngredientsType>
// bool BilayerCreator<IngredientsType>::addLipidUp(int32_t x,int32_t y,int32_t z)
// {
// 
//     MoveAddScMonomer trialMove;
// 
//     typename IngredientsType::molecules_type& molecules=ingredients_.modifyMolecules();
// 
//     //add the lipids by setting the position of the trial move
//     //and applying the move to the system using trialMove.apply(...)
//     for(size_t n=0;n<lipidTemplateUp.size();n++)
//     {
//         trialMove.setPosition(lipidTemplateUp[n]+VectorInt3(x,y,z));
//         if(n==5 || n==6 || n==7)
//             trialMove.setType(getHeadTag());
//         else
//             trialMove.setType(getTailTag());
// 
//         //add the monomer to this position. the position is not checked again here
//         //it must be checked before
//         trialMove.apply(ingredients_);
//         //connect the lipid monomers
//         if(n!=0 && n!=8)
//             molecules.connect(molecules.size()-1,molecules.size()-2);
// 
//     }
// 
//     molecules.connect(molecules.size()-1,molecules.size()-8);
//     return true;
// 
// }
// 
// 
// 
// ////////////////////////////////////////////////////////////////////////////////
// //addLipidDown: adds a lipid according to lipidTemplateDown to position x,y,z
// ////////////////////////////////////////////////////////////////////////////////
// template<class IngredientsType>
// bool BilayerCreator<IngredientsType>::addLipidDown(int32_t x,int32_t y,int32_t z)
// {
//     //procedure like in addLipidUp
//     MoveAddScMonomer trialMove;
//     typename IngredientsType::molecules_type& molecules=ingredients_.modifyMolecules();
// 
//     for(size_t n=0;n<lipidTemplateDown.size();n++)
//     {
//         trialMove.setPosition(lipidTemplateDown[n]+VectorInt3(x,y,z));
//         if(n==5 || n==6 || n==7)
//             trialMove.setType(getHeadTag());
//         else
//             trialMove.setType(getTailTag());
// 
//         //add the monomer to this position. the position is not checked again here
//         //it must be checked before
//         trialMove.apply(ingredients_);
//         //connect the lipid monomers
//         if(n!=0 && n!=8)
//             molecules.connect(molecules.size()-1,molecules.size()-2);
// 
//     }
// 
//     molecules.connect(molecules.size()-1,molecules.size()-8);
//     return true;
// }
// 
// 
// ////////////////////////////////////////////////////////////////////////////////
// //findFreeLipidSites: looks for possible positions in the plane z=boxZ/2, where
// //a pair of lipids can be inserted (one pointing upwards, one downwards and
// //90 deg rotated around long axix). these positions are saved in the vector
// //freeSites. the positions are looked for on a grid with spacing of four lattice
// //units.
// ////////////////////////////////////////////////////////////////////////////////
// template<class IngredientsType>
// void BilayerCreator<IngredientsType>::findFreeLipidSites()
// {
//     //first empty the freeSites vector
//     freeSites.clear();
//     //now look for free sites using MoveAddScMonomer
//     MoveAddScMonomer trialMove;
// 
//     int32_t minLipidSpacing=4;
// 
//     //go through the grid of x,y positions and use the trial move
//     //to check if all monomers of a lipid could be inserted there
//     for(int32_t x=1; x<ingredients_.getBoxX();x+=minLipidSpacing)
//     {
//         for(int32_t y=1;y<ingredients_.getBoxY();y+=minLipidSpacing)
//         {
//             VectorInt3 centerPosition(x,y,ingredients_.getBoxZ()/2);
//             bool accepted=true;
// 
//             for(size_t n=0;n<lipidTemplateUp.size();n++)
//             {
//                 if(n==5 || n==6 || n==7)
//                     trialMove.setType(getHeadTag());
//                 else
//                     trialMove.setType(getTailTag());
// 
//                 //check the positions here, using the check function of the move
//                 trialMove.resetProbability();
//                 trialMove.setPosition(centerPosition+lipidTemplateUp[n]);
//                 accepted=accepted && trialMove.check(ingredients_);
// 
//                 trialMove.resetProbability();
//                 trialMove.setPosition(centerPosition+lipidTemplateDown[n]);
//                 accepted=accepted && trialMove.check(ingredients_);
//             }
// 
//             if(accepted==true)
//                 freeSites.push_back(centerPosition);
// 
//         }
//     }
// 
//     std::cout<<"BilayerCreator::findFreeLipidSites(): found "<<freeSites.size()<<" possible positions (each for 2 lipids)\n";
// }
// 

#endif /*BILAYER_CREATOR_H*/