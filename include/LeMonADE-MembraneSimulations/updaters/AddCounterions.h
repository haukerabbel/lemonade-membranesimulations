#ifndef ADD_COUNTERIONS_H
#define ADD_COUNTERIONS_H

#include <stdint.h>
#include <cmath>
#include <cstdlib>

#include <LeMonADE/updater/AbstractUpdater.h>
#include <LeMonADE/utility/Vector3D.h>
#include <LeMonADE/updater/moves/MoveAddMonomerSc.h>



/*****************************************************************************
 *this updater adds ions of given number and charge to the system. the form
 *of the molecules, as well as the attribute types of the monomers, can be
 *controlled by giving a template as argument (template not in the sense of <>)
 *it works with standard bfm only (simple cubic lattice), because it
 *uses MoveAddScMonomer to insert monomers into the system.
 *****************************************************************************/


template<class IngredientsType>
class AddCounterions:public AbstractUpdater
{
public:
	AddCounterions(IngredientsType& ing,uint32_t numberOfIons,int32_t charge,int32_t attribute);
	~AddCounterions(){}
	
	typedef typename IngredientsType::molecules_type molecules_type;
	
	//general updater functions
	bool execute(){return false;}
	void initialize();
	void cleanup(){};
	
private:
	bool addIon();
	
	IngredientsType& ingredients;
	uint32_t nIons;
	int32_t ionCharge;
	int32_t ionAttribute;
	
};


////////////////////////////////////////////////////////////////////////////////
//constuctor: sets up lipid templates
////////////////////////////////////////////////////////////////////////////////
template<class IngredientsType>
AddCounterions<IngredientsType>::AddCounterions(IngredientsType& ing, uint32_t numberOfIons,int32_t charge,int32_t attribute)
:ingredients(ing)
,nIons(numberOfIons)
,ionCharge(charge)
,ionAttribute(attribute)
{	
}

////////////////////////////////////////////////////////////////////////////////
//initialize: adds the ions
////////////////////////////////////////////////////////////////////////////////
template<class IngredientsType>
void AddCounterions<IngredientsType>::initialize()
{

	int32_t insertedIons=0;
	int32_t failedAttempts=0;
	while(insertedIons<nIons && failedAttempts<10000)
	{
		if(addIon()==true)
			insertedIons++;
		else
			failedAttempts++;
	}
	if(insertedIons==nIons)
	{
		std::cout<<"AddCounterions::execute(): successfully added "<<nIons<<" ions to the system\n";
		
	}
	else
	{
		std::stringstream errormessage;
		errormessage<<"AddCounterions::execute():"
		            <<"unable to add the requested number of "<<nIons
		            <<" counterions. the inserted number was "<<insertedIons<<std::endl;
			    throw std::runtime_error(errormessage.str());
	}
	
}

////////////////////////////////////////////////////////////////////////////////
//addIon: adds an ion according to ionTemplate starting from position x,y,z
////////////////////////////////////////////////////////////////////////////////
template<class IngredientsType>
bool AddCounterions<IngredientsType>::addIon()
{

    MoveAddMonomerSc trialMove;
    molecules_type& molecules=ingredients.modifyMolecules();

    size_t currentSize=molecules.size();
    int32_t x=std::rand()%ingredients.getBoxX();
    int32_t y=std::rand()%ingredients.getBoxY();
    int32_t z=std::rand()%ingredients.getBoxZ();

    bool accepted=true;    
    
    trialMove.setPosition(VectorInt3(x,y,z));
    trialMove.setTag(ionAttribute);
    accepted=trialMove.check(ingredients);
    
    
    if(!accepted) return false;
    else
    {	
	    trialMove.apply(ingredients);
	    molecules[molecules.size()-1].setCharge(ionCharge);
	    return true;
    }

}


#endif /*ADD_COUNTERIONS_H*/