#ifndef CHARGE_MONOMER_GROUP_H
#define CHARGE_MONOMER_GROUP_H

#include <vector>
#include <stdint.h>

#include <LeMonADE/updater/AbstractUpdater.h>
#include <LeMonADE/utility/Vector3D.h>
#include <LeMonADE/utility/MonomerGroup.h>
#include <LeMonADE/utility/DepthIterator.h>
#include <LeMonADE/utility/DepthIteratorPredicates.h>


/*****************************************************************************
 * this updater adds charges to a group of monomers. 
 * the user can choose how many charges will be distributed, the value of
 * the charge used, and how the charges will be distributed (randomly or
 * regularly spaced with offset from beginning of the group)
 *****************************************************************************/


template<class IngredientsType>
class ChargeMonomerGroup:public AbstractUpdater
{
public:
	typedef typename IngredientsType::molecules_type molecules_type;
	
	ChargeMonomerGroup(IngredientsType& ing,std::vector<int32_t> monoTypes,std::vector<int32_t> chargeDist);
	ChargeMonomerGroup(IngredientsType& ing,std::vector<int32_t> monoTypes,uint32_t nChargedResidues,uint32_t chargeVal,bool randomDist=true);
	~ChargeMonomerGroup(){}
	
	//general updater functions
	bool execute(){return false;}
	void initialize();
	void cleanup(){};
	
private:
	
	IngredientsType& ingredients;
	std::vector<int32_t> chargeVector;
	std::vector<int32_t> groupMonomerTypes;
	bool chargeVectorGiven,randomDistGiven,alternatingDistGiven;
	uint32_t nCharges;
	int32_t chargeValue;
};

template<class IngredientsType>
ChargeMonomerGroup<IngredientsType>::ChargeMonomerGroup(IngredientsType& ing,std::vector<int32_t> monoTypes,std::vector<int32_t> chargeDist)
:ingredients(ing)
,chargeVector(chargeDist)
,groupMonomerTypes(monoTypes)
,chargeVectorGiven(true)
,randomDistGiven(false)
,alternatingDistGiven(false)
,nCharges(chargeDist.size())
,chargeValue(0)
{
}

template<class IngredientsType>
ChargeMonomerGroup<IngredientsType>::ChargeMonomerGroup(IngredientsType& ing,std::vector<int32_t> monoTypes,uint32_t nChargedResidues,uint32_t chargeVal,bool randomDist)
:ingredients(ing)
,groupMonomerTypes(monoTypes)
,chargeVectorGiven(false)
,randomDistGiven(randomDist)
,alternatingDistGiven(!randomDist)
,nCharges(nChargedResidues)
,chargeValue(chargeVal)
{
}

template<class IngredientsType>
void ChargeMonomerGroup<IngredientsType>::initialize()
{
	//find the monomer groups
	std::vector<MonomerGroup<typename IngredientsType::molecules_type> > monomerGroups;
	fill_connected_groups(ingredients.getMolecules(),monomerGroups,MonomerGroup<typename IngredientsType::molecules_type>(&(ingredients.getMolecules())),hasType(groupMonomerTypes));
	
	if(monomerGroups.size()==0) 
		std::cerr<<"Warning: ChargeMonomerGroup::initialize(): no monomers selected\n";
	
	for(size_t n=0;n<monomerGroups.size();n++){
		if(monomerGroups[n].size()<nCharges)
			throw std::runtime_error("ChargeMonomerGroup::execute()...monomer group smaller than requested number of charges\n");
		if(monomerGroups[n].size()!=chargeVector.size() && chargeVectorGiven==true)
			throw std::runtime_error("ChargeMonomerGroup::execute()...monomer group and vector of charges have different size\n");
	
		if(chargeVectorGiven){
			for(size_t m=0;m<chargeVector.size();m++)
				ingredients.modifyMolecules()[monomerGroups[n].trueIndex(m)].setCharge(chargeVector[m]);
		}
		else if(randomDistGiven){
			std::vector<uint32_t> availMonos;
			for(uint32_t idx=0;idx<monomerGroups[n].size();idx++)
				availMonos.push_back(monomerGroups[n].trueIndex(idx));
			
			for(uint32_t m=0;m<nCharges;m++){
				uint32_t randomIdx=std::rand()%availMonos.size();
				uint32_t randomMono=availMonos[randomIdx];
				ingredients.modifyMolecules()[randomMono].setCharge(chargeValue);
				availMonos[randomIdx]=availMonos[availMonos.size()-1];
				availMonos.pop_back();
			}
		}
		else
		{
			int32_t spacing=monomerGroups[n].size()/nCharges;
			int32_t offset=(monomerGroups[n].size()-spacing*(nCharges-1))/2;
			uint32_t chargeCount=0;
			for(int32_t m=offset;m<monomerGroups[n].size();m+=spacing){
				uint32_t idx=monomerGroups[n].trueIndex(m);
				ingredients.modifyMolecules()[idx].setCharge(chargeValue);
				chargeCount++;
			}
			if(chargeCount!=nCharges)
				throw std::runtime_error("ChargeMonomerGroup::execute()...regular charge dist leads to wrong number of charges\n");
		}
		
	}
}



#endif /*CHARGE_MONOMER_GROUP_H*/