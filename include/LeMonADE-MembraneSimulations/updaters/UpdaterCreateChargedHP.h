#ifndef UPDATER_CREATE_CHARGED_HP_H
#define UPDATER_CREATE_CHARGED_HP_H

#include<vector>

#include <LeMonADE/updater/UpdaterAbstractCreate.h>

template<class IngredientsType>
class UpdaterCreateChargedHP:public UpdaterAbstractCreate<IngredientsType>
{
public:
	UpdaterCreateChargedHP(IngredientsType& ing, int32_t type,uint32_t length,int32_t charge,uint32_t npolymers=1);
	~UpdaterCreateChargedHP(){}
	
	virtual void initialize();
	virtual bool execute(){return false;}
	virtual void cleanup(){};
	
	
private:
	IngredientsType& ingredients;
	int32_t monoType,polymerCharge;
	uint32_t polymerLength,nPolymers;
	
	using UpdaterAbstractCreate<IngredientsType>::addSingleMonomer;
	using UpdaterAbstractCreate<IngredientsType>::addMonomerToParent;
	
};



template<class IngredientsType>
UpdaterCreateChargedHP<IngredientsType>::UpdaterCreateChargedHP(IngredientsType& ing, 
								int32_t type, 
								uint32_t length, 
								int32_t charge, 
								uint32_t npolymers)
:UpdaterAbstractCreate<IngredientsType>(ing)
,ingredients(ing)
,monoType(type)
,polymerLength(length)
,polymerCharge(charge)
,nPolymers(npolymers)
{
}

template<class IngredientsType>
void UpdaterCreateChargedHP<IngredientsType>::initialize()
{
	std::vector<size_t> chainEnds(nPolymers,0);
	std::vector<int32_t> charges(polymerLength,0);
	
	//generate charge distribution
	if(polymerCharge*polymerCharge>polymerLength*polymerLength){
		std::stringstream errormessage;
		errormessage<<"UpdaterCreateChargedHP::more charges than monomers";
		throw std::runtime_error(errormessage.str());
	}
	else{
		uint32_t nCharges= polymerCharge>=0 ? uint32_t(polymerCharge) : uint32_t(-polymerCharge);
		if(nCharges>0){
			uint32_t chargeDist=polymerLength/nCharges;
			uint32_t chargeOffset=(polymerLength-(1+(nCharges-1)*chargeDist))/2;
			
			for(uint32_t c=0;c<nCharges;c++){
				charges[chargeOffset+c*chargeDist]=polymerCharge/int32_t(nCharges);
			}
		}
	}
	
	
	
	for(uint32_t n=0;n<nPolymers;n++){
		//create initial monomer
		if(addSingleMonomer(monoType)){
			chainEnds[n]=ingredients.getMolecules().size()-1;
			ingredients.modifyMolecules()[chainEnds[n]].setCharge(charges[0]);
		}
		else{
			std::stringstream errormessage;
			errormessage<<"UpdaterCreateChargedHP::error adding initial monomer for chain no "<<n;
			throw std::runtime_error(errormessage.str());
		}
		
		
		for(uint32_t l=1;l<polymerLength;l++){
			
			//create initial monomer
			if(addMonomerToParent(chainEnds[n],monoType)){
				chainEnds[n]=ingredients.getMolecules().size()-1;
				ingredients.modifyMolecules()[chainEnds[n]].setCharge(charges[l]);
			}
			else{
				std::stringstream errormessage;
				errormessage<<"UpdaterCreateChargedHP::error adding monomer "<<l<<"to chain no "<<n;
				throw std::runtime_error(errormessage.str());
			}
			
		}
	}
	
	ingredients.synchronize();
}

#endif /*include guard*/