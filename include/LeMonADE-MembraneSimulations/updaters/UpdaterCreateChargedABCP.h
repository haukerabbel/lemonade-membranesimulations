#ifndef UPDATER_CREATE_CHARGED_ABCP_H
#define UPDATER_CREATE_CHARGED_ABCP_H

#include<cmath>
#include<vector>

#include <LeMonADE/updater/UpdaterAbstractCreate.h>

template<class IngredientsType>
class UpdaterCreateChargedABCP:public UpdaterAbstractCreate<IngredientsType>
{
public:
	UpdaterCreateChargedABCP(IngredientsType& ing,
				 int32_t typeA,
			  int32_t typeB,
			  uint32_t lengthA,
			  uint32_t lengthB,
			  int32_t chargeA,
			  int32_t chargeB,
			  uint32_t nblocks,
			  uint32_t npolymers=1);
	
	~UpdaterCreateChargedABCP(){}
	
	virtual void initialize();
	virtual bool execute(){return false;}
	virtual void cleanup(){};
	
	
private:
	IngredientsType& ingredients;
	int32_t monoTypeA,monoTypeB,blockChargeA,blockChargeB;
	uint32_t blockLengthA,blockLengthB,nBlocks,nPolymers;
	
	using UpdaterAbstractCreate<IngredientsType>::addSingleMonomer;
	using UpdaterAbstractCreate<IngredientsType>::addMonomerToParent;
	
};



template<class IngredientsType>
UpdaterCreateChargedABCP<IngredientsType>::UpdaterCreateChargedABCP(IngredientsType& ing,
								    int32_t typeA,
								    int32_t typeB,
								    uint32_t lengthA,
								    uint32_t lengthB,
								    int32_t chargeA,
								    int32_t chargeB,
								    uint32_t nblocks,
								    uint32_t npolymers)
:UpdaterAbstractCreate<IngredientsType>(ing)
,ingredients(ing)
,monoTypeA(typeA)
,monoTypeB(typeB)
,blockLengthA(lengthA)
,blockLengthB(lengthB)
,blockChargeA(chargeA)
,blockChargeB(chargeB)
,nBlocks(nblocks)
,nPolymers(npolymers)
{
}

template<class IngredientsType>
void UpdaterCreateChargedABCP<IngredientsType>::initialize()
{
	std::vector<size_t> chainEnds(nPolymers,0);
	std::vector<int32_t> monoTypes;
	std::vector<int32_t> charges;
	
	//generate polymer type sequence
	if(blockChargeA*blockChargeA>blockLengthA*blockLengthA || blockChargeB*blockChargeB>blockLengthB*blockLengthB){
		std::stringstream errormessage;
		errormessage<<"UpdaterCreateChargedABCP::more charges than monomers in block";
		throw std::runtime_error(errormessage.str());
	}
	else{
		std::vector<int32_t> chargeSeriesBlockA(blockLengthA,0);
		std::vector<int32_t> chargeSeriesBlockB(blockLengthB,0);
		uint32_t nChargesA= std::abs(blockChargeA);
		uint32_t nChargesB= std::abs(blockChargeB);
		
		if(nChargesA>0){
			uint32_t chargeDistA=blockLengthA/nChargesA;
			uint32_t chargeOffsetA=(blockLengthA-(1+(nChargesA-1)*chargeDistA))/2;
			
			for(uint32_t c=0;c<nChargesA;c++){
				chargeSeriesBlockA[chargeOffsetA+c*chargeDistA]=blockChargeA/int32_t(nChargesA);
			}
		}
		if(nChargesB>0){
			uint32_t chargeDistB=blockLengthB/nChargesB;
			uint32_t chargeOffsetB=(blockLengthB-(1+(nChargesB-1)*chargeDistB))/2;
			
			for(uint32_t c=0;c<nChargesB;c++){
				chargeSeriesBlockB[chargeOffsetB+c*chargeDistB]=blockChargeB/int32_t(nChargesB);
			}
		}
		for(size_t n=0;n<nBlocks;n++){
			for(size_t a=0;a<blockLengthA;a++){
				monoTypes.push_back(monoTypeA);
				charges.push_back(chargeSeriesBlockA[a]);
			}
			for(size_t b=0;b<blockLengthB;b++){
				monoTypes.push_back(monoTypeB);
				charges.push_back(chargeSeriesBlockB[b]);
			}
		}
	}
	
	
	//add initial monomers
	for(uint32_t n=0;n<nPolymers;n++){
		//create initial monomer
		if(addSingleMonomer(monoTypes[0])){
			chainEnds[n]=ingredients.getMolecules().size()-1;
			ingredients.modifyMolecules()[chainEnds[n]].setCharge(charges[0]);
		}
		else{
			std::stringstream errormessage;
			errormessage<<"UpdaterCreateChargedABCP::error adding initial monomer for chain no "<<n;
			throw std::runtime_error(errormessage.str());
		}
		
		
		for(uint32_t l=1;l<monoTypes.size();l++){
			
			//create initial monomer
			if(addMonomerToParent(chainEnds[n],monoTypes[l])){
				chainEnds[n]=ingredients.getMolecules().size()-1;
				ingredients.modifyMolecules()[chainEnds[n]].setCharge(charges[l]);
			}
			else{
				std::stringstream errormessage;
				errormessage<<"UpdaterCreateChargedABCP::error adding monomer "<<l<<"to chain no "<<n;
				throw std::runtime_error(errormessage.str());
			}
			
		}
	}
	ingredients.synchronize();
}

#endif /*include guard*/