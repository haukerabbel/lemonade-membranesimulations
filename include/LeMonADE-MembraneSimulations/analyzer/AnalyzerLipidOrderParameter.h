#ifndef ANALYZER_LIPID_ORDER_PARAMETER_H
#define ANALYZER_LIPID_ORDER_PARAMETER_H

#include <LeMonADE/analyzer/AbstractAnalyzer.h>
#include <LeMonADE/utility/MonomerGroup.h>
#include <LeMonADE/utility/ResultFormattingTools.h>

#include <LeMonADE-Extensions/utilities/Histogram1D.h>

#include <LeMonADE-MembraneSimulations/analyzer/BilayerAnalyzer.h>
#include <LeMonADE-MembraneSimulations/utilities/MonomerTypes.h>

template <class IngredientsType>
class AnalyzerLipidOrderParameter: public BilayerAnalyzer<IngredientsType>
{
public:
    AnalyzerLipidOrderParameter(const IngredientsType& i,int32_t lowerLimit=10000000, int32_t upperLimit=-1)
	:BilayerAnalyzer<IngredientsType>(i)
	,molecules(i.getMolecules())
	,minMcsLimit(lowerLimit)
	,maxMcsLimit(upperLimit)
	,nsamples(0)
	,orderParameter(3,std::vector<double>(3,0.0))
	{
		
	}
	
	virtual ~AnalyzerLipidOrderParameter(){};
	
	virtual void initialize();
	virtual bool execute();
	virtual void cleanup();
    
private:
    
	typedef typename BilayerAnalyzer<IngredientsType>::molecules_type molecules_type;
	using BilayerAnalyzer<IngredientsType>::getIngredients;
	using BilayerAnalyzer<IngredientsType>::getLipids;
	
	VectorDouble3 calcEndToEndVector(const MonomerGroup<molecules_type>&)const;
	std::vector<std::vector<double> > orderParameter;
	uint32_t nsamples;
	
	const molecules_type& molecules;
	
	const int32_t minMcsLimit;
	const int32_t maxMcsLimit;
	
};


template<class IngredientsType>
bool AnalyzerLipidOrderParameter<IngredientsType>::execute()
{
	if(molecules.getAge()<minMcsLimit) return true;
	if(maxMcsLimit>-1 && molecules.getAge()>maxMcsLimit) return true;
	
	//update com etc
	BilayerAnalyzer<IngredientsType>::execute();
	
	const std::vector<MonomerGroup<molecules_type> >& lipids=getLipids();
	
	uint32_t nlipidsCounted=0;
	double Q00=0.0;
	double Q11=0.0;
	double Q22=0.0;
	double Q01=0.0;
	double Q02=0.0;
	double Q12=0.0;
	
	for(size_t n=0;n<lipids.size();n++){
		
		VectorDouble3 b=calcEndToEndVector(lipids[n]);
		b.normalize();
		
		if(isnan(b[0])||isnan(b[1])||isnan(b[2])) continue;
		else{
			//diagonal elements
			Q00+=(3.0*b[0]*b[0]-1.0);
			Q11+=(3.0*b[1]*b[1]-1.0);
			Q22+=(3.0*b[2]*b[2]-1.0);
			
			//off-diagonal elements
			Q01+=(3.0*b[0]*b[1]);
			Q02+=(3.0*b[0]*b[2]);
			Q12+=(3.0*b[1]*b[2]);
			
			nlipidsCounted++;
		}
	}
	
	if(nlipidsCounted>0){
		nsamples++;
		
		orderParameter[0][0]+=Q00/(2.0*double(nlipidsCounted));
		orderParameter[1][1]+=Q11/(2.0*double(nlipidsCounted));
		orderParameter[2][2]+=Q22/(2.0*double(nlipidsCounted));
		orderParameter[0][1]+=Q01/(2.0*double(nlipidsCounted));
		orderParameter[1][0]+=Q01/(2.0*double(nlipidsCounted));
		orderParameter[0][2]+=Q02/(2.0*double(nlipidsCounted));
		orderParameter[2][0]+=Q02/(2.0*double(nlipidsCounted));
		orderParameter[1][2]+=Q12/(2.0*double(nlipidsCounted));
		orderParameter[2][1]+=Q12/(2.0*double(nlipidsCounted));
	}
	return true;
	
	
// 	for(size_t n=0;n<lipids.size();n++){
// 		VectorDouble3 b=calcEndToEndVector(lipids[n]);
// 		b.normalize();
// 		if(isnan(b[0])||isnan(b[1])||isnan(b[2])) continue;
// 		//diagonal elements
// 		orderParameter[0][0]+=(3.0*b[0]*b[0]-1.0)/(2.0*double(lipids.size()));
// 		orderParameter[1][1]+=(3.0*b[1]*b[1]-1.0)/(2.0*double(lipids.size()));
// 		orderParameter[2][2]+=(3.0*b[2]*b[2]-1.0)/(2.0*double(lipids.size()));
// 		
// 		//off-diagonal elements
// 		double Q01=(3.0*b[0]*b[1])/(2.0*double(lipids.size()));
// 		double Q02=(3.0*b[0]*b[2])/(2.0*double(lipids.size()));
// 		double Q12=(3.0*b[1]*b[2])/(2.0*double(lipids.size()));
// 		
// 		orderParameter[0][1]+=Q01;orderParameter[1][0]+=Q01;
// 		orderParameter[0][2]+=Q02;orderParameter[2][0]+=Q02;
// 		orderParameter[2][1]+=Q12;orderParameter[1][2]+=Q12;
// 		
// 		
// 	}
// 	
// 
// 	nsamples++;
// 	return true;
}


template<class IngredientsType>
void AnalyzerLipidOrderParameter<IngredientsType>::cleanup()
{
	std::stringstream comment;
	comment<<"file produced by analyzer AnalyzerLipidOrderParameter\n"
		<<"format:lipid tensorial order parameter (matrix)"
		<<"number of lipids analyzed "<<getLipids().size()<<std::endl
		<<"number of frames analyzed "<<nsamples<<std::endl;
		
	std::vector<std::vector<double> > results=orderParameter;
	for(size_t n=0;n<results.size();n++){
		for(size_t m=0;m<results[n].size();m++){
			results[n][m]/=double(nsamples);
		}
	}
	
		
	ResultFormattingTools::writeResultFile("LipidOrderParameter.dat",getIngredients(),results,comment.str());
}

template<class IngredientsType>
void AnalyzerLipidOrderParameter<IngredientsType>::initialize()
{
	BilayerAnalyzer<IngredientsType>::initialize();
}


template<class IngredientsType>
VectorDouble3 AnalyzerLipidOrderParameter<IngredientsType>::calcEndToEndVector(const MonomerGroup< molecules_type >& lipid) const
{
	VectorDouble3 top(0.0,0.0,0.0);
	VectorDouble3 bottom(0.0,0.0,0.0);
	
	for(size_t n=0;n<lipid.size();n++){
		if(lipid[n].getAttributeTag()==MonomerTypesCPU::LIPID_HEAD &&
			molecules.getNumLinks(lipid.trueIndex(n))==3)
			top=lipid[n];
		else if(lipid[n].getAttributeTag()==MonomerTypesCPU::LIPID_TAIL &&
			molecules.getNumLinks(lipid.trueIndex(n))==1)
			bottom+=lipid[n];
	}
	
	VectorDouble3 result=top-0.5*bottom;
	return result;
}

#endif /*ANALYZER_LIPID_ORDER_PARAMETER*/
