#ifndef ANALYZER_BILAYER_PROFILE_H
#define ANALYZER_BILAYER_PROFILE_H

#include<map>
#include<utility>

#include<LeMonADE/utility/Vector3D.h>
#include<LeMonADE/utility/DepthIterator.h>
#include<LeMonADE/utility/MonomerGroup.h>
#include<LeMonADE/utility/DepthIteratorPredicates.h>
#include<LeMonADE/utility/DistanceCalculation.h>

#include<LeMonADE-Extensions/utilities/Histogram1D.h>

#include<LeMonADE-MembraneSimulations/analyzer/BilayerAnalyzer.h>

/******************************************************************************
 * this analyzer is for a single ABA triblock copolymer on a membrane
 * it calculates the z-histogram free energy for the B block and the total
 * polymer as a function of |z| 
 * ***************************************************************************/

template <class IngredientsType>
class AnalyzerBilayerProfile: public BilayerAnalyzer<IngredientsType>
{
public:
	AnalyzerBilayerProfile(const IngredientsType& i,double binwidth, int32_t lowerLimit=10000000, int32_t upperLimit=-1)
	:BilayerAnalyzer<IngredientsType>(i)
	,molecules(i.getMolecules())
	,nFrames(0)
	,minMcsLimit(lowerLimit)
	,maxMcsLimit(upperLimit)
	,binWidth(binwidth)
	{
	}
	
	virtual ~AnalyzerBilayerProfile(){};
	
	virtual void initialize();
	virtual bool execute();
	virtual void cleanup();
	
private:
	
	typedef typename BilayerAnalyzer<IngredientsType>::molecules_type molecules_type;
	using BilayerAnalyzer<IngredientsType>::getIngredients;
	using BilayerAnalyzer<IngredientsType>::getLipids;
	//using BilayerAnalyzer<IngredientsType>::getBilayerCom;
	
	std::map<int32_t,Histogram1D > profiles;
	std::map<int32_t, Histogram1D > profilesSymmetric;
	
	const molecules_type& molecules;
	
	uint32_t nFrames;
	
	const int32_t minMcsLimit;
	const int32_t maxMcsLimit;
	const double binWidth;
	
};


template<class IngredientsType>
bool AnalyzerBilayerProfile<IngredientsType>::execute()
{
	if(molecules.getAge()<minMcsLimit) return true;
	if(maxMcsLimit>-1 && molecules.getAge()>maxMcsLimit) return true;
	
	//this updates the center of mass info of the bilayer
	BilayerAnalyzer<IngredientsType>::execute();
	
	double zDist;
	
	for(size_t n=0;n<molecules.size();n++)
	{
		//if type has not occured yet,add it to distanceDistribution
		if(profiles.count(molecules[n].getAttributeTag())==0)
		{
			
			profiles.insert(std::make_pair(molecules[n].getAttributeTag(),
						       Histogram1D(-double(getIngredients().getBoxZ())/2.0,double(getIngredients().getBoxZ())/2.0,uint32_t(double(getIngredients().getBoxZ())/binWidth))));
			profilesSymmetric.insert(std::make_pair(molecules[n].getAttributeTag(),
								Histogram1D(-double(getIngredients().getBoxZ())/2.0,double(getIngredients().getBoxZ())/2.0,uint32_t(double(getIngredients().getBoxZ())/binWidth))));
			
			std::cout<<"adding new columns for additional attribute tag. number of columns "<<molecules[n].getAttributeTag()<<std::endl;
			
		}
		
		//then add a count to the histogram
		zDist=Lemonade::calcDistanceVector3D(VectorDouble3(molecules[n]),BilayerAnalyzer<IngredientsType>::getBilayerCom(molecules[n]),getIngredients()).getZ();
		
		try{
			profiles.find(molecules[n].getAttributeTag())->second.addValue(zDist);
			profilesSymmetric.find(molecules[n].getAttributeTag())->second.addValue(zDist,0.5);
			profilesSymmetric.find(molecules[n].getAttributeTag())->second.addValue(-zDist,0.5);
		}
		catch(std::runtime_error& e)
		{
			std::stringstream errormessage;
			errormessage<<"Exception caught in BilayerProfile::execute on adding histogram value at "<<zDist<<std::endl;
			errormessage<<"Original error\n"<<e.what()<<std::endl;
			throw std::runtime_error(errormessage.str());
			
		}
		
		
	}
	nFrames++;
	return true;
}


template<class IngredientsType>
void AnalyzerBilayerProfile<IngredientsType>::cleanup()
{
	//collect information about types being used
	std::stringstream formatString;
	formatString<<"format:z ";
	
	double dBoxX=double(getIngredients().getBoxX());
	double dBoxY=double(getIngredients().getBoxY());
	double layerWidth=profiles.begin()->second.getBinwidth();
	
	std::vector< std::vector <double> > result;
	
	result.push_back(profiles.begin()->second.getVectorBins());
	std::map<int32_t,Histogram1D>::iterator it;
	for(it=profiles.begin();it!=profiles.end();++it)
	{
		result.push_back(it->second.getVectorValues());
		formatString<<"density(attribute="<<it->first<<") ";
	}
	for (size_t n=0;n<result[0].size();n++)
	{
		for(size_t m=1;m<result.size();m++){
			result[m][n]/=(double(nFrames)*dBoxX*dBoxY*layerWidth);
			result[m][n]*=8.0; //for volume fraction
		}
	}
	
	std::stringstream comment;
	std::stringstream filename;
	
	comment<<"file produced by analyzer BilayerProfile\n"
	<<formatString.str()<<"\n"
	<<"bin width was "<<binWidth<<"\n";
	
	filename<<"BilayerDensityProfile_binwidth"<<binWidth<<".dat";
	ResultFormattingTools::writeResultFile(filename.str(),getIngredients(),result, comment.str());
	
	
	
	//////// symmetric ///////
	std::vector< std::vector <double> > resultSym;
	
	resultSym.push_back(profilesSymmetric.begin()->second.getVectorBins());
	for(it=profilesSymmetric.begin();it!=profilesSymmetric.end();++it)
	{
		resultSym.push_back(it->second.getVectorValues());
	}
	
	
	
	for (size_t n=0;n<resultSym[0].size();n++)
	{
		for(size_t m=1;m<resultSym.size();m++){
			resultSym[m][n]/=(double(nFrames)*dBoxX*dBoxY*layerWidth); //normalize to volume
			resultSym[m][n]*=8.0; //factor for bfm volume fraction
		}
		
	}
	
	std::stringstream commentSym;
	std::stringstream filenameSym;
	commentSym<<"file produced by analyzer BilayerProfile\n"
	<<formatString.str()<<"\n"
	<<"bin width was "<<binWidth<<"\n"
	<<"profile has been made symmetric\n";
	
	filenameSym<<"BilayerDensityProfileSymmetric_binwidth"<<binWidth<<".dat";
	ResultFormattingTools::writeResultFile(filenameSym.str(),getIngredients(),resultSym, commentSym.str());
}

template<class IngredientsType>
void AnalyzerBilayerProfile<IngredientsType>::initialize()
{
	BilayerAnalyzer<IngredientsType>::initialize();
}

#endif
