#ifndef BILAYER_ANALYZER_H
#define BILAYER_ANALYZER_H


/*****************************************************************************
 *base class for analyzation of lipid bilayers. provides some functions
 *that are convenient for this purpose.
 *****************************************************************************/
#include<utility>
#include<map>

#include<LeMonADE/utility/Vector3D.h>
#include<LeMonADE/utility/DepthIterator.h>
#include<LeMonADE/utility/MonomerGroup.h>
#include<LeMonADE/utility/DepthIteratorPredicates.h>
#include<LeMonADE/utility/DistanceCalculation.h>
#include<LeMonADE/analyzer/AbstractAnalyzer.h>


template<class IngredientsType>
class BilayerAnalyzer:public AbstractAnalyzer
{
public:
	BilayerAnalyzer(const IngredientsType& ing,double patchSize=16.0);
	virtual ~BilayerAnalyzer(){};
	
	typedef typename IngredientsType::molecules_type molecules_type;
	
	virtual void initialize();
	virtual bool execute();
	virtual void cleanup(){};
	
	void setSquarePatchSize(double size){binSize=size;}
	void addLipidTag(int32_t tag){lipidTags.push_back(tag);}
	
protected:
	
	const IngredientsType& getIngredients() const {return ingredients;}
	const std::vector<MonomerGroup<molecules_type> >& getLipids() const {return lipids;}
	const MonomerGroup<molecules_type>& getLipidMonomers() const {return lipidMonomers;}
	
	//center of mass of all bilayer monomers
	VectorDouble3 getBilayerCom();
	//center of mass of square patch of bilayer which position belongs to
	VectorDouble3 getBilayerCom(VectorDouble3 patchPosition);
	//center of mass of bilayer patch with radius patchSize around patchPosition. slow
	VectorDouble3 getBilayerPatchCOM(VectorDouble3 patchPosition,double patchSize);
	//center of mass of a monomer group
	VectorDouble3 centerOfMass(const MonomerGroup<molecules_type>& group);
	
private:
	
	double foldBack(double coordinate, double boxsize) const{
		if(coordinate<0.0) while(coordinate<0.0) coordinate+=boxsize;
		else while(coordinate>=boxsize) coordinate-=boxsize;
		return coordinate;
	}
	
	//get the bin number labelling the bilayer square patch of coordinate
	int32_t getBin(double coordinate,double boxSize){return int32_t(foldBack(coordinate,boxSize)/binSize);}
	//reset the pre-calculated center of mass values
	void refreshCom();
	
	const IngredientsType& ingredients;
	//lipids, sorted by individual lipid molecules
	std::vector< MonomerGroup<molecules_type> > lipids;
	//lipid monomers in one group (not sorted by lipid molecules)
	MonomerGroup<molecules_type> lipidMonomers;
	//contains the center of mass of the lipid bilayer at age bilayerCOMage
	VectorDouble3 bilayerCom;
	//contains a map of index pairs to bilayer COM
	std::vector<std::vector < VectorDouble3> >bilayerComAt;
	//size of patches the bilayer is devided into for mapping to bilayerComAt
	double binSize;
	
	//define attribute tags
	std::vector<int32_t> lipidTags;
};




/////////////////////////////////////////////////////////////////////////////

template<class IngredientsType>
BilayerAnalyzer<IngredientsType>::BilayerAnalyzer(const IngredientsType& ing,double patchSize)
    :ingredients(ing),lipidMonomers(ing.getMolecules()),binSize(patchSize)
{
}

template<class IngredientsType>
void BilayerAnalyzer<IngredientsType>::initialize()
{
	if(lipidTags.size()==0){lipidTags.push_back(1);lipidTags.push_back(2);}
	//set up lipid group
	fill_connected_groups(ingredients.getMolecules(),lipids,MonomerGroup<molecules_type>(ingredients.getMolecules()),hasType(lipidTags));
	//set up lipidMonomers group (contains all monomers belonging to lipids)
	std::set<int32_t> lipidTagsSet(lipidTags.begin(),lipidTags.end());
	for(size_t n=0;n<ingredients.getMolecules().size();n++)
	{
		if(lipidTagsSet.count(ingredients.getMolecules()[n].getAttributeTag())==1)
			lipidMonomers.push_back(n);
	}
	
	std::cout<<"Initialized BilayerAnalyzer: nLipids="<<lipids.size()<<" lipid size="<<lipids[0].size()<<std::endl;
	std::cout<<"Initialized BilayerAnalyzer: nLipidMonomers="<<lipidMonomers.size()<<std::endl;
	
	//initialize the center of mass
	refreshCom();
	
}

template<class IngredientsType>
bool BilayerAnalyzer<IngredientsType>::execute()
{
	refreshCom();
}


template<class IngredientsType>
VectorDouble3 BilayerAnalyzer<IngredientsType>::getBilayerCom()
{
	return bilayerCom;
}

template<class IngredientsType>
VectorDouble3 BilayerAnalyzer<IngredientsType>::getBilayerCom(VectorDouble3 patchPosition)
{
	int32_t xBin=getBin(patchPosition.getX(),ingredients.getBoxX());
	int32_t yBin=getBin(patchPosition.getY(),ingredients.getBoxY());

 	return bilayerComAt[xBin][yBin];
}
///////////////////////////////////////////////////////////////////////////////
//returns the center of mass of a bilayer patch around the x-y position of
//patchPosition with radius patchSize
///////////////////////////////////////////////////////////////////////////////
template<class IngredientsType>
VectorDouble3 BilayerAnalyzer<IngredientsType>::getBilayerPatchCOM(VectorDouble3 patchPosition, double patchSize)
{
	//this will contain all monomers of the patch
	MonomerGroup<molecules_type> patch(getIngredients().getMolecules());

	//if a lipid is within the radius, add all its monomers to the patch
	for(size_t n=0;n<lipidMonomers.size();n++)
	{
		VectorDouble3 monoPosition=lipidMonomers[n];
		
		VectorDouble3 distVector=Lemonade::calcDistanceVector3D(patchPosition,monoPosition,getIngredients());
		if(distVector.getX()*distVector.getX()+distVector.getY()*distVector.getY()<=patchSize*patchSize)
			patch.push_back(lipidMonomers.trueIndex(n));
		
	}

	if(patch.size()==0)
	{
		std::stringstream errormessage;
		errormessage<<"BilayerAnalyzerYachong::getBilayerPatchCOM: no monomers in patch! You may want to choose a larger patch.\n";
		throw std::runtime_error(errormessage.str());
	}
	else if(patch.size()<( lipids[0].size()* 10 ))
	{
		std::cerr<<"WARNING: BilayerAnalyzerYachong::getBilayerPatchCOM(): few lipids in patch. You may want to choose a larger patch.\n";
	}
	
	return centerOfMass(patch);
}

///////////////////////////////////////////////////////////////////////////////
template<class IngredientsType>
void BilayerAnalyzer<IngredientsType>::refreshCom()
{

	bilayerComAt.clear();
	bilayerCom=centerOfMass(lipidMonomers);
	
	typedef std::vector< std::vector<MonomerGroup<molecules_type> > > patches_type;
	typedef typename patches_type::iterator patches_iterator; 
	
	patches_type patches;
	int32_t maxBinsX=int32_t(ingredients.getBoxX()/binSize);
	int32_t maxBinsY=int32_t(ingredients.getBoxY()/binSize);
	patches.resize(maxBinsX);
	bilayerComAt.resize(maxBinsX);
	for(int32_t x=0;x<patches.size();++x){
		bilayerComAt[x].resize(maxBinsY);
		for(int32_t y=0;y<maxBinsY;++y)
			patches[x].push_back(MonomerGroup<molecules_type>(ingredients.getMolecules()));
	}
		
	
			
	
	for(size_t n=0;n<lipidMonomers.size();n++)
	{
		int32_t xBin=getBin(lipidMonomers[n].getX(),ingredients.getBoxX());
		int32_t yBin=getBin(lipidMonomers[n].getY(),ingredients.getBoxY());
		
		patches[xBin][yBin].push_back(n);
	}
	

	for(patches_iterator it=patches.begin();it!=patches.end();++it){
		
	}
	for(int32_t x=0;x<patches.size();++x)
		for(int32_t y=0;y<maxBinsY;++y)
			bilayerComAt[x][y]=centerOfMass(patches[x][y]);
			

}

template<class IngredientsType>
VectorDouble3 BilayerAnalyzer<IngredientsType>::centerOfMass(const MonomerGroup<molecules_type>& group)
{
	VectorDouble3 COM(0.0,0.0,0.0);
	for(size_t n=0;n<group.size();n++)
		COM+=group[n];
		
	COM=COM/double(group.size());
	
// 	//second iteration
	VectorDouble3 COM_corrected(0.0,0.0,0.0);
	int32_t N=0;
	for(size_t n=0;n<group.size();n++){
		if(std::fabs(COM.getZ()-double(group[n].getZ()))<ingredients.getBoxZ()/2.0){
			COM_corrected+=group[n];
			N++;
		}
	}
	
	return COM_corrected/double(N);
	
}

#endif /*BILAYER_ANALYZER_H*/
