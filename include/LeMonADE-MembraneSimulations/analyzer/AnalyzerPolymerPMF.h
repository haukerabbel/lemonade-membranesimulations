#ifndef POTENTIALOFMEANFORCE_Z_H
#define POTENTIALOFMEANFORCE_Z_H

#include<vector>
#include <LeMonADE/utility/ResultFormattingTools.h>
#include <LeMonADE/utility/DistanceCalculation.h>

#include <LeMonADE-Extensions/utilities/Histogram1D.h>

#include <LeMonADE-MembraneSimulations/analyzer/BilayerAnalyzer.h>


/* ****************************************************************************
 * makes a histogram of the center of mass position of the groups given to
 * the constructor with respect to either a bilayer patch around its x-y coordinates,
 * or with respect to the complete bilayer (depending on the option set in constructor.
 * The template argument BinningType selects either a binning that takes the
 * absolute value (ModulusBinning), or the signed value (SymmetricBinning) of
 * the distance.
 **************************************************************************** */

template<class IngredientsType>
class AnalyzerPolymerPMF: public BilayerAnalyzer<IngredientsType>
{
private:
	
	using BilayerAnalyzer<IngredientsType>::getIngredients;
	using BilayerAnalyzer<IngredientsType>::getLipids;
	using BilayerAnalyzer<IngredientsType>::getBilayerCom;
	using BilayerAnalyzer<IngredientsType>::getBilayerPatchCOM;
	
	typedef typename BilayerAnalyzer<IngredientsType>::molecules_type molecules_type;
	typedef MonomerGroup <const molecules_type> group_type;
	
	const molecules_type& molecules;
	group_type polymer;
	std::set<int32_t> monoTypes;
	
	Histogram1D spacialDistribution;
	Histogram1D spacialDistributionSymmetric;
	
	const int32_t minMcsLimit;
	const int32_t maxMcsLimit;
	
	int32_t calculationType;
	
	std::string outputFile;
	
	VectorDouble3 centerOfMass(const group_type& group) const;
	
public:
	
	enum CALC_TYPES{ALL_COM=1,REGION_COM=2,PATCH_COM=3};
	
	AnalyzerPolymerPMF(const IngredientsType& i,
			   std::set<int32_t> types,
		    int32_t calcType=ALL_COM,
		    std::string outputFilename="polymer_PMF.dat",
		    int32_t lowerLimit=10000000,
		    int32_t upperLimit=-1);
	
	virtual ~AnalyzerPolymerPMF(){}
	
	virtual void initialize();
	virtual bool execute();
	virtual void cleanup();
	
};

///////////////// implementation of members //////////////////////////////////

template<class IngredientsType>
AnalyzerPolymerPMF<IngredientsType>::AnalyzerPolymerPMF(
	const IngredientsType& i,
	std::set<int32_t> types,
	int32_t calcType,
	std::string outputFilename,
	int32_t lowerLimit,
	int32_t upperLimit)
:BilayerAnalyzer<IngredientsType>(i)
,monoTypes(types)
,calculationType(calcType)
,molecules(i.getMolecules())
,minMcsLimit(lowerLimit)
,maxMcsLimit(upperLimit)
,polymer(i.getMolecules())
,outputFile(outputFilename)
{
	
}

template<class IngredientsType>
void AnalyzerPolymerPMF<IngredientsType>::initialize()
{
	double binWidth=1.0;
	
	BilayerAnalyzer<IngredientsType>::initialize();
	spacialDistribution.reset(-double(getIngredients().getBoxZ())/2.0,
				  double(getIngredients().getBoxZ())/2.0,
				  uint32_t(double(getIngredients().getBoxZ())/binWidth));
	
	spacialDistributionSymmetric.reset(-double(getIngredients().getBoxZ())/2.0,
				  double(getIngredients().getBoxZ())/2.0,
				  uint32_t(double(getIngredients().getBoxZ())/binWidth));
	
	for(size_t n=0;n<molecules.size();n++){
		if(monoTypes.count(getIngredients().getMolecules()[n].getAttributeTag())==1)
			polymer.push_back(n);
	}
	
	if(polymer.size()>0){
		std::cout<<"AnalyzerPolymerPMF::initialize(): found group size "<<polymer.size()<<std::endl;
	}
	else{
		std::stringstream errormessage;
		errormessage<<"AnalyzerPolymerPMF:initialize()...group is empty";
		throw std::runtime_error(errormessage.str());
	}
	
	switch(calculationType){
		case(ALL_COM):
			break;
		case(PATCH_COM):
			outputFile=std::string("patch_")+outputFile;
			break;
		case(REGION_COM):
			outputFile=std::string("regionCom_")+outputFile;
			break;
			
	}
}

///////////////////////////////////////////////////////////////////////////
//collects values...the calculation is done in cleanup
///////////////////////////////////////////////////////////////////////////
template<class IngredientsType>
bool AnalyzerPolymerPMF<IngredientsType>::execute()
{
	BilayerAnalyzer<IngredientsType>::execute();
	
	if(molecules.getAge()<minMcsLimit) return true;
	if(maxMcsLimit>-1 && molecules.getAge()>maxMcsLimit) return true;
	
	//center of mass, z-distance, and bin for the groups
	VectorDouble3 groupCOM;
	
	groupCOM=centerOfMass(polymer);
	
	VectorDouble3 referencePoint;
	//get the reference point depending on which calc type was chosen
	switch(calculationType){
		case(ALL_COM):
			referencePoint=getBilayerCom();
			break;
		case(PATCH_COM):
			referencePoint=getBilayerPatchCOM(groupCOM,16); //patch size 16
			break;
		case(REGION_COM):
			referencePoint=getBilayerCom(groupCOM);
			break;
		default:
			throw std::runtime_error("AnalyzerPolymerPMF: invalid calc type");
			
	}
	
	//calculate the recuced distance in z-direction from reference point to
	//group center of mass
	double distance=Lemonade::calcDistanceVector3D(referencePoint,groupCOM,getIngredients()).getZ();
	
	spacialDistribution.addValue(distance,1.0);
	spacialDistributionSymmetric.addValue(distance,0.5);
	spacialDistributionSymmetric.addValue(-distance,0.5);
	
	return true;
}

//////////////////////////////////////////////////////////////////////////////
//get the histogram results and make file output in the cleanup() method
//////////////////////////////////////////////////////////////////////////////
template<class IngredientsType>
void AnalyzerPolymerPMF<IngredientsType>::cleanup()
{
	//results are written in vector<vector<double>> so that WriteResultFile
	//can be used
	std::vector< std::vector <double> > results;
	std::vector< std::vector <double> > resultsSymmetric;
	
	results.push_back(spacialDistribution.getVectorBins());
	resultsSymmetric.push_back(spacialDistribution.getVectorBins());
	
	results.push_back(spacialDistribution.getVectorValuesNormalized());
	resultsSymmetric.push_back(spacialDistributionSymmetric.getVectorValuesNormalized());
	
	results.push_back(spacialDistribution.getVectorValues());
	resultsSymmetric.push_back(spacialDistributionSymmetric.getVectorValues());
	
	std::vector<double> tmpNRGvector;
	std::vector<double> tmpNRGvectorSymmetric;
	double tmpNRG;
		
	for(size_t m=0;m<results.rbegin()->size();m++)
	{
		if( (*results.rbegin())[m] >0.0) tmpNRG=-log((*results.rbegin())[m]);
		else tmpNRG=0.0;
		tmpNRGvector.push_back(tmpNRG);
		
		if( (*resultsSymmetric.rbegin())[m] >0.0) tmpNRG=-log((*resultsSymmetric.rbegin())[m]);
		else tmpNRG=0.0;
		tmpNRGvectorSymmetric.push_back(tmpNRG);
	}
		
	results.push_back(tmpNRGvector);
	resultsSymmetric.push_back(tmpNRGvectorSymmetric);
		
	
	std::stringstream comment;
	comment<<"Created by AnalyzerPolymerPMF\n";
	comment<<"group size: "<<polymer.size()<<std::endl;
	comment<<"number of lipids: "<<getLipids().size()<<std::endl;
	comment<<"file analyzed from mcs "<<minMcsLimit;
	if (maxMcsLimit>-1) comment<<" to mcs "<<maxMcsLimit;
	comment<<std::endl;
	comment<<"distance distribution molecule-bilayer. format: distance d, prob,nrg=-log(prob),nCounts \n";
	
	
	ResultFormattingTools::writeResultFile(outputFile,getIngredients(),results,comment.str());
	ResultFormattingTools::writeResultFile(std::string("symmetric_")+outputFile,getIngredients(),resultsSymmetric,comment.str());
}

template<class IngredientsType>
VectorDouble3 AnalyzerPolymerPMF<IngredientsType>::centerOfMass(const group_type& group) const
{
	VectorDouble3 COM(0.0,0.0,0.0);
	for(size_t n=0;n<group.size();n++)
		COM+=group[n];
		
	COM=COM/double(group.size());
	return COM;	
}


#endif /*POTENTIALOFMEANFORCE_Z_H*/
