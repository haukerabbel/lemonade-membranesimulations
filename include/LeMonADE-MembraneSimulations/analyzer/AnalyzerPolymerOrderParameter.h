#ifndef ANALYZER_POLYMER_ORDER_PARAMETER_H
#define ANALYZER_POLYMER_ORDER_PARAMETER_H



/*
 * analyzer calculates number of monomers of the polymer, which are in their
 * preferred environment, both for A and B monomers
 * */
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

#include<LeMonADE/utility/Vector3D.h>
#include<LeMonADE/utility/MonomerGroup.h>
#include<LeMonADE/utility/ResultFormattingTools.h>
#include<LeMonADE/analyzer/AbstractAnalyzer.h>

#include<LeMonADE-MembraneSimulations/utilities/MonomerTypes.h>

template<class IngredientsType>
class AnalyzerPolymerOrderParameter:public AbstractAnalyzer
{
public:
	AnalyzerPolymerOrderParameter(const IngredientsType& ing,std::string filename,int64_t minMcs=10000000);
	virtual ~AnalyzerPolymerOrderParameter();
	
	virtual bool execute();
	virtual void initialize();
	virtual void cleanup();
	
private:
	typedef typename IngredientsType::molecules_type molecules_type;
	std::vector <std::vector < uint32_t > > orderParameter;
	MonomerGroup<const molecules_type> polymer;
	
	const IngredientsType& ingredients;
	const int64_t minAnalyzeMcs;
	const std::string outputFile;
	
	uint nMonosA,nMonosB;
	std::vector<VectorInt3> contactSites;
};


/************ implementation ******************/


template<class IngredientsType>
AnalyzerPolymerOrderParameter<IngredientsType>::AnalyzerPolymerOrderParameter(const IngredientsType& ing,std::string filename,int64_t minMcs)
:ingredients(ing)
,minAnalyzeMcs(minMcs)
,outputFile(filename)
,polymer(ing.getMolecules())
,nMonosA(0),nMonosB(0)
{
	orderParameter.resize(5);
}

///////////////////////////////////////////////////////////////////////////////
//free memory in destructor
template<class IngredientsType>
AnalyzerPolymerOrderParameter<IngredientsType>::~AnalyzerPolymerOrderParameter()
{
	
}


template<class IngredientsType>
bool AnalyzerPolymerOrderParameter<IngredientsType>::execute()
{
	if(ingredients.getMolecules().getAge()<minAnalyzeMcs) return true;
	orderParameter[0].push_back(ingredients.getMolecules().getAge());
	orderParameter[2].push_back(nMonosA);
	orderParameter[4].push_back(nMonosB);
	
	orderParameter[1].push_back(0);
	orderParameter[3].push_back(0);
	
	
	VectorInt3 pos;
	
	//loop through all polymer
	for(size_t n=0;n<polymer.size();n++)
	{
		int32_t type=polymer[n].getAttributeTag();
		pos=polymer[n];
		uint32_t contactsHead=0;
		uint32_t contactsTail=0;
		
		for(size_t contactNo=0;contactNo<contactSites.size();contactNo++)
		{
			uint32_t latticeEntry=uint32_t(ingredients.getLatticeEntry(pos+contactSites[contactNo]));
			if(latticeEntry==MonomerTypesCPU::LIPID_HEAD) contactsHead++;
			else if(latticeEntry==MonomerTypesCPU::LIPID_TAIL) contactsTail++;
			
		}
		//hydrophilic
		if(type==MonomerTypesCPU::POLYMER_A && contactsHead>contactsTail) *(orderParameter[1].rbegin())+=1;
			
		else if(type==MonomerTypesCPU::POLYMER_B && contactsTail>contactsHead) *(orderParameter[3].rbegin())+=1;
	}
	
	
	return true;
}


////////////////////////////////////////////////////////////////////////////////
//creates the output files
template<class IngredientsType>
void AnalyzerPolymerOrderParameter<IngredientsType>::initialize()
{
	//prepare the contact shell, in which the contacts are counted
	
	contactSites.push_back(VectorInt3(2,0,0));
	contactSites.push_back(VectorInt3(2,1,0));
	contactSites.push_back(VectorInt3(2,0,1));
	contactSites.push_back(VectorInt3(2,1,1));
	contactSites.push_back(VectorInt3(0,2,0));
	contactSites.push_back(VectorInt3(1,2,0));
	contactSites.push_back(VectorInt3(0,2,1));
	contactSites.push_back(VectorInt3(1,2,1));
	contactSites.push_back(VectorInt3(0,0,2));
	contactSites.push_back(VectorInt3(1,0,2));
	contactSites.push_back(VectorInt3(0,1,2));
	contactSites.push_back(VectorInt3(1,1,2));
	
	contactSites.push_back(VectorInt3(-1,0,0));
	contactSites.push_back(VectorInt3(-1,1,0));
	contactSites.push_back(VectorInt3(-1,0,1));
	contactSites.push_back(VectorInt3(-1,1,1));
	contactSites.push_back(VectorInt3(0,-1,0));
	contactSites.push_back(VectorInt3(1,-1,0));
	contactSites.push_back(VectorInt3(0,-1,1));
	contactSites.push_back(VectorInt3(1,-1,1));
	contactSites.push_back(VectorInt3(0,0,-1));
	contactSites.push_back(VectorInt3(1,0,-1));
	contactSites.push_back(VectorInt3(0,1,-1));
	contactSites.push_back(VectorInt3(1,1,-1));
	
	
	nMonosA=0;
	nMonosB=0;
	//loop through all particles
	for(size_t n=0;n<ingredients.getMolecules().size();n++)
	{
		if(ingredients.getMolecules()[n].getAttributeTag()==MonomerTypesCPU::POLYMER_A){
			nMonosA++;
			polymer.push_back(n);
		}
		else if(ingredients.getMolecules()[n].getAttributeTag()==MonomerTypesCPU::POLYMER_B){
			nMonosB++;
			polymer.push_back(n);
		}
		
	}

	
	std::cout<<"AnalyzerPolymerOrderParameter: monomers of type A:"<<nMonosA<<", B:"<<nMonosB<<std::endl;
	
}

///////////////////////////////////////////////////////////////////////////////
//writes the rest of the data and closes the output files
template<class IngredientsType>
void AnalyzerPolymerOrderParameter<IngredientsType>::cleanup()
{
	
	std::stringstream comment;
	comment<<"Created by AnalyzerPolymerOrderParameter\n";
	comment<<"format:\tmcs HydrophilicInPreferred NHydrophilic HydrophobicInPreferred NHydrophobic";
	
	ResultFormattingTools::writeResultFile(outputFile,ingredients,orderParameter, comment.str());
	
}



#endif // ANALYZER_POLYMER_ORDER_PARAMETER_H
