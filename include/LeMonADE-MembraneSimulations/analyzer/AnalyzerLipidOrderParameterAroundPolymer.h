#ifndef ANALYZER_LIPID_ORDER_PARAMETER_AROUND_POLYMER_H
#define ANALYZER_LIPID_ORDER_PARAMETER_AROUND_POLYMER_H

#include <LeMonADE/analyzer/AbstractAnalyzer.h>
#include <LeMonADE/utility/MonomerGroup.h>
#include <LeMonADE/utility/ResultFormattingTools.h>
#include <LeMonADE/utility/DistanceCalculation.h>
#include <LeMonADE/utility/DepthIterator.h>
#include <LeMonADE/utility/DepthIteratorPredicates.h>

#include <LeMonADE-Extensions/utilities/Histogram1D.h>

#include <LeMonADE-MembraneSimulations/analyzer/BilayerAnalyzer.h>
#include <LeMonADE-MembraneSimulations/utilities/MonomerTypes.h>

template <class IngredientsType>
class AnalyzerLipidOrderParameterAroundPolymer: public BilayerAnalyzer<IngredientsType>
{
public:
    AnalyzerLipidOrderParameterAroundPolymer(const IngredientsType& i,double radius=20.0,int32_t lowerLimit=10000000, int32_t upperLimit=-1)
	:BilayerAnalyzer<IngredientsType>(i)
	,molecules(i.getMolecules())
	,minMcsLimit(lowerLimit)
	,maxMcsLimit(upperLimit)
	,nsamples(0)
	,orderParameter(3,std::vector<double>(3,0.0))
	,maxDist(radius)
	,polymer(i.getMolecules())
	{
		
	}
	
	virtual ~AnalyzerLipidOrderParameterAroundPolymer(){};
	
	virtual void initialize();
	virtual bool execute();
	virtual void cleanup();
    
private:
    
	typedef typename BilayerAnalyzer<IngredientsType>::molecules_type molecules_type;
	using BilayerAnalyzer<IngredientsType>::getIngredients;
	using BilayerAnalyzer<IngredientsType>::getLipids;
	
	VectorDouble3 calcEndToEndVector(const MonomerGroup<molecules_type>&)const;
	std::vector<std::vector<double> > orderParameter;
	uint32_t nsamples;
	
	const molecules_type& molecules;
	
	const int32_t minMcsLimit;
	const int32_t maxMcsLimit;
	
	double maxDist;
	
	MonomerGroup<molecules_type> polymer;
	
};


template<class IngredientsType>
bool AnalyzerLipidOrderParameterAroundPolymer<IngredientsType>::execute()
{
	if(molecules.getAge()<minMcsLimit) return true;
	if(maxMcsLimit>-1 && molecules.getAge()>maxMcsLimit) return true;
	
	//update com etc
	BilayerAnalyzer<IngredientsType>::execute();
	
	const std::vector<MonomerGroup<molecules_type> >& lipids=getLipids();
	VectorDouble3 polymerCom=BilayerAnalyzer<IngredientsType>::centerOfMass(polymer);
	
	uint32_t nlipidsCounted=0;
	double Q00=0.0;
	double Q11=0.0;
	double Q22=0.0;
	double Q01=0.0;
	double Q02=0.0;
	double Q12=0.0;
	
	for(size_t n=0;n<lipids.size();n++){
		
		VectorDouble3 lipidCom=BilayerAnalyzer<IngredientsType>::centerOfMass(lipids[n]);
		VectorDouble3 distanceVector=Lemonade::calcDistanceVector3D(polymerCom,lipidCom,getIngredients());
		VectorDouble3 b=calcEndToEndVector(lipids[n]);
		b.normalize();
		
		if(distanceVector[0]*distanceVector[0]+distanceVector[1]*distanceVector[1] > maxDist*maxDist) continue;
		else if(isnan(b[0])||isnan(b[1])||isnan(b[2])) continue;
		else{
			//diagonal elements
			Q00+=(3.0*b[0]*b[0]-1.0);
			Q11+=(3.0*b[1]*b[1]-1.0);
			Q22+=(3.0*b[2]*b[2]-1.0);
			
			//off-diagonal elements
			Q01+=(3.0*b[0]*b[1]);
			Q02+=(3.0*b[0]*b[2]);
			Q12+=(3.0*b[1]*b[2]);
			
			nlipidsCounted++;
		}
	}
	
	if(nlipidsCounted>0){
		nsamples++;
		
		orderParameter[0][0]+=Q00/(2.0*double(nlipidsCounted));
		orderParameter[1][1]+=Q11/(2.0*double(nlipidsCounted));
		orderParameter[2][2]+=Q22/(2.0*double(nlipidsCounted));
		orderParameter[0][1]+=Q01/(2.0*double(nlipidsCounted));
		orderParameter[1][0]+=Q01/(2.0*double(nlipidsCounted));
		orderParameter[0][2]+=Q02/(2.0*double(nlipidsCounted));
		orderParameter[2][0]+=Q02/(2.0*double(nlipidsCounted));
		orderParameter[1][2]+=Q12/(2.0*double(nlipidsCounted));
		orderParameter[2][1]+=Q12/(2.0*double(nlipidsCounted));
	}
	return true;
}


template<class IngredientsType>
void AnalyzerLipidOrderParameterAroundPolymer<IngredientsType>::cleanup()
{
	std::stringstream comment;
	comment<<"file produced by analyzer AnalyzerLipidOrderParameterAroundPolymer\n"
		<<"format:lipid tensorial order parameter (matrix)"
		<<"number of lipids analyzed "<<getLipids().size()<<std::endl
		<<"number of frames analyzed "<<nsamples<<std::endl;
		
	std::vector<std::vector<double> > results=orderParameter;
	for(size_t n=0;n<results.size();n++){
		for(size_t m=0;m<results[n].size();m++){
			results[n][m]/=double(nsamples);
		}
	}
	
	std::stringstream filename;
	filename<<"LipidOrderParameter_polymerDistance"<<maxDist<<".dat";	
	ResultFormattingTools::writeResultFile(filename.str(),getIngredients(),results,comment.str());
}

template<class IngredientsType>
void AnalyzerLipidOrderParameterAroundPolymer<IngredientsType>::initialize()
{
	BilayerAnalyzer<IngredientsType>::initialize();
	for(size_t n=0;n<molecules.size();n++){
		if(molecules[n].getAttributeTag()==MonomerTypesCPU::POLYMER_A ||
			molecules[n].getAttributeTag()==MonomerTypesCPU::POLYMER_B
		)
		{
			polymer.push_back(n);
		}
	}
	
	if(polymer.size()==0){
		std::stringstream errormessage;
		errormessage<<"AnalyzerLipidOrderParameterAroundPolymer: no polymer found\n";
		throw std::runtime_error(errormessage.str());
	}
}


template<class IngredientsType>
VectorDouble3 AnalyzerLipidOrderParameterAroundPolymer<IngredientsType>::calcEndToEndVector(const MonomerGroup< molecules_type >& lipid) const
{
	VectorDouble3 top(0.0,0.0,0.0);
	VectorDouble3 bottom(0.0,0.0,0.0);
	
	for(size_t n=0;n<lipid.size();n++){
		if(lipid[n].getAttributeTag()==MonomerTypesCPU::LIPID_HEAD &&
			molecules.getNumLinks(lipid.trueIndex(n))==3)
			top=lipid[n];
		else if(lipid[n].getAttributeTag()==MonomerTypesCPU::LIPID_TAIL &&
			molecules.getNumLinks(lipid.trueIndex(n))==1)
			bottom+=lipid[n];
	}
	
	VectorDouble3 result=top-0.5*bottom;
	return result;
}

#endif /*ANALYZER_LIPID_ORDER_PARAMETER_AROUND_POLYMER*/
