#ifndef ANALYZER_TRANSLOCATION_PROFILE_H
#define ANALYZER_TRANSLOCATION_PROFILE_H

#include<cmath>

#include <LeMonADE/utility/ResultFormattingTools.h>
#include <LeMonADE/utility/Vector3D.h>
#include <LeMonADE/utility/DepthIteratorPredicates.h>
#include <LeMonADE/utility/MonomerGroup.h>

#include <LeMonADE-Extensions/utilities/Histogram1D.h>

#include <LeMonADE-MembraneSimulations/analyzer/BilayerAnalyzer.h>
/************************************************************************
 * calculates translocation of molecules across membrane with respect to
 * a given reference point
 * **********************************************************************/


template < class IngredientsType > class AnalyzerTranslocationProfile : public BilayerAnalyzer<IngredientsType>
{
	using BilayerAnalyzer<IngredientsType>::getIngredients;
	using BilayerAnalyzer<IngredientsType>::getBilayerCom;
	
	typedef typename BilayerAnalyzer<IngredientsType>::molecules_type molecules_type;
	
	std::vector<MonomerGroup<molecules_type> > groups;
	MonomerGroup<molecules_type> polymer;
	std::vector<int32_t> previousZone;
	std::vector<int32_t> attributes;
	std::vector<double> zone_two_distance;
	std::vector<std::vector<int32_t> > zoneHistory;
	
	Histogram1D translocationProfile;
	double barrier1,barrier2;
	
	void registerTranslocation(size_t n);
	double foldBack(double value, int32_t box) const; 
	int32_t getCurrentZone(double coordinate);
	
	VectorDouble3 referencePosition() const{return centerOfMass(polymer);}
	
	uint64_t samplesAnalzed;
	uint64_t startAge,endAge;
	
	template < class MoleculesType > VectorDouble3 centerOfMass(const MoleculesType& m) const
	{
		VectorDouble3 CoM_sum;    
		for ( uint i = 0; i < m.size(); ++i)
		{
			CoM_sum.setX( CoM_sum.getX() + m[i].getX() );
			CoM_sum.setY( CoM_sum.getY() + m[i].getY() );
			CoM_sum.setZ( CoM_sum.getZ() + m[i].getZ() );
		}
		
		double inv_N = 1.0 / double ( m.size() );
		
		VectorDouble3 CoM (
			double ( CoM_sum.getX() ) * inv_N, 
				   double ( CoM_sum.getY() ) * inv_N,
				   double ( CoM_sum.getZ() ) * inv_N);
		
		return CoM;
		
	}
public:
	
	AnalyzerTranslocationProfile(const IngredientsType& ing, std::vector<int32_t> attr,double b1,double b2);
	virtual ~AnalyzerTranslocationProfile(){}
	
	virtual void initialize();
	virtual bool execute();
	virtual void cleanup();
};

/*************************************************************************
 * implementation of memeber execute()
 * ***********************************************************************/


template<class IngredientsType>
AnalyzerTranslocationProfile<IngredientsType>::AnalyzerTranslocationProfile(const IngredientsType& ing, 
									    std::vector<int32_t> attr,
									    double b1=-10.0,
									    double b2=10.0)
:BilayerAnalyzer<IngredientsType>(ing)
,polymer(ing.getMolecules())
,barrier1(b1)
,barrier2(b2)
,attributes(attr)
,samplesAnalzed(0)
,startAge(0)
,endAge(0)
{
}


template<class IngredientsType>
void AnalyzerTranslocationProfile<IngredientsType>::initialize()
{
	BilayerAnalyzer<IngredientsType>::initialize();
	
	fill_connected_groups(getIngredients().getMolecules(),groups,MonomerGroup<molecules_type>(getIngredients().getMolecules()),hasType(attributes));
	zone_two_distance.resize(groups.size(),0.0);
	previousZone.resize(groups.size(),0);
	zoneHistory.resize(groups.size());
	for(size_t n=0;n<zoneHistory.size();n++){
		zoneHistory[n].resize(3,0);
	}
	
	double binWidth=1.0;
	translocationProfile.reset(0.0,
				   double(getIngredients().getBoxX()),
				   uint32_t(double(getIngredients().getBoxX())/binWidth));
	
	for(size_t n=0;n<getIngredients().getMolecules().size();n++){
		if(getIngredients().getMolecules()[n].getAttributeTag()==4 ||
			getIngredients().getMolecules()[n].getAttributeTag()==5)
		{
			polymer.push_back(n);
			
		}
	}
	
	std::cout<<"AnalyzerTranslocationProfile::initialize():\n";
	std::cout<<"\t limits are "<<barrier1<<"\t"<<barrier2<<"\n";
	std::cout<<"\t number of molecules analyzed "<<groups.size()<<"\n";
	
	startAge=getIngredients().getMolecules().getAge();
}

template< class IngredientsType >
bool AnalyzerTranslocationProfile<IngredientsType>::execute()
{
	BilayerAnalyzer<IngredientsType>::execute();
	
	for(size_t n=0;n<groups.size();n++)
	{
		VectorDouble3 groupCOM=centerOfMass(groups[n]);
		
		int32_t zone=getCurrentZone(groupCOM.getZ());
		if(zone==2){
			VectorDouble3 d=Lemonade::calcDistanceVector3D(groupCOM,referencePosition(),getIngredients());
			double rad=std::sqrt(d.getX()*d.getX()+d.getY()*d.getY());
			zone_two_distance[n]=rad;
		}
			
		if(zone!=previousZone[n]){
			zoneHistory[n].push_back(zone);
			zoneHistory[n].erase(zoneHistory[n].begin());
			previousZone[n]=zone;
			if(zoneHistory[n][1]==2){
				if(zoneHistory[n][0]==1 && zoneHistory[n][2]==3) registerTranslocation(n);
				else if(zoneHistory[n][0]==3 && zoneHistory[n][2]==1) registerTranslocation(n);
			}

		}
	}
	
	samplesAnalzed++;
	if(samplesAnalzed%1000==0) cleanup();
	
	endAge=getIngredients().getMolecules().getAge();
	
	return true;
}

template<class IngredientsType>
void AnalyzerTranslocationProfile<IngredientsType>::cleanup()
{	
	//now write to file (including a comment)
	std::stringstream comment;
	comment<<"Translocation of molecules.\n";
	comment<<"sample size: "<<groups.size()<<" groups\n";
	
	std::stringstream filename;
	filename<<"translocations_types";
	for (int32_t t=0;t<attributes.size();t++) filename<<"_"<<attributes[t];
	filename<<".dat";
	
	std::vector<std::vector<double> > trans;
	trans.push_back(translocationProfile.getVectorBins());
	trans.push_back(translocationProfile.getVectorValues());
	std::vector<double> startTime(trans[0].size(),double(startAge));
	std::vector<double> endTime(trans[0].size(),double(endAge));
	trans.push_back(startTime);
	trans.push_back(endTime);
	
	
	ResultFormattingTools::writeResultFile(filename.str(),getIngredients(),trans,comment.str());
	
}

template<class IngredientsType>
void AnalyzerTranslocationProfile<IngredientsType>::registerTranslocation(size_t n)
{
// 	VectorDouble3 pos=centerOfMass(groups[n]);
// 	VectorDouble3 d=Lemonade::calcDistanceVector3D(pos,referencePosition(),getIngredients());
// 	double rad=std::sqrt(d.getX()*d.getX()+d.getY()*d.getY());
	//double average_rad=zone_two_distance[n].first/zone_two_distance[n].second;
	translocationProfile.addValue(zone_two_distance[n],1.0);
}

template<class IngredientsType>
double AnalyzerTranslocationProfile<IngredientsType>::foldBack(double value, int32_t box) const {
	
	//make coordinate positive, so that modulo is properly defined
	while (value < 0.0) {
		value += double(box);
	}
	while (value >= double(box)) {
		value -= double(box);
	}
	return value;
}

template<class IngredientsType>
int32_t AnalyzerTranslocationProfile<IngredientsType>::getCurrentZone(double coordinate){
	double d=coordinate-getBilayerCom().getZ();
	Lemonade::reduceDistanceInPeriodicSpace(d,getIngredients().getBoxZ());
	//std::cout<<"coord "<<coordinate<<"\t bilayer "<<getBilayerCom().getZ()<<"\t d "<<d<<std::endl;
	if(d<barrier1) return 1; //below bilayer
	else if(d<barrier2) return 2;//in bilayer
	else return 3;//above bilayer
}

#endif


