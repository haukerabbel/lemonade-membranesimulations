#ifndef FEATURE_BILAYER_DISTANCE_POTENTIAL_H
#define FEATURE_BILAYER_DISTANCE_POTENTIAL_H

#include <LeMonADE/feature/Feature.h>
#include <LeMonADE/feature/FeatureAttributes.h>
#include <LeMonADE/feature/FeatureBoltzmann.h>
#include <LeMonADE/updater/moves/MoveBase.h>
#include <LeMonADE/updater/moves/MoveLocalSc.h>
#include <LeMonADE/utility/DistanceCalculation.h>

#include <LeMonADE-MembraneSimulations/utilities/MonomerTypes.h>



template<class PotentialType>
class FeatureBilayerDistancePotential:public Feature
{
public:
	
	typedef LOKI_TYPELIST_1(FeatureBoltzmann) required_features_back;
	typedef LOKI_TYPELIST_1(FeatureAttributes) required_features_front;
	
	FeatureBilayerDistancePotential()
	:distanceBilayerGroup(0.0)
	{}
	
	virtual ~FeatureBilayerDistancePotential(){}
    
	const PotentialType& getPotential(){return potential;}
	PotentialType& modifyPotential(){return potential;}
	//the name is chosen so it can work with UpdaterAdaptiveUmbrellaSampling
	double getCurrentReactionCoordinate() const {return distanceBilayerGroup;}
	
	void addAffectedMonomerType(int32_t type){affectedTypes.insert(type);}
	
	//for all unknown moves: does nothing
	template < class IngredientsType> 
	bool checkMove( const IngredientsType& ingredients, const MoveBase& move ) const;

	//overload for MoveLocalSc
	template < class IngredientsType> 
	bool checkMove( const IngredientsType& ingredients, MoveLocalSc& move );

	//for unknown moves(does nothing)
	template<class IngredientsType> 
	void applyMove(IngredientsType& ing, const MoveBase& move){}
	
	//for moves of type MoveLocalBase
	template<class IngredientsType> 
	void applyMove(IngredientsType& ing, const MoveLocalSc& move);

	//synchronize: sets up the potential
	template<class IngredientsType>
	void synchronize(IngredientsType& ingredients);
private:
	
	PotentialType potential;
	std::set<int32_t> affectedTypes;
	std::vector<size_t> affectedMonomerGroup;
	std::vector<size_t> lipids;
	double distanceBilayerGroup; 
	
};



///////////////////////////////////////////////////////////////////////////////
////////////////////Implementation of methods////////////////////////////


template<class PotentialType>
template<class IngredientsType>
bool FeatureBilayerDistancePotential<PotentialType>::checkMove(const IngredientsType& ingredients, const MoveBase& move) const
{
	return true;//nothing to do for unknown moves
}


template<class PotentialType>
template<class IngredientsType>
bool FeatureBilayerDistancePotential<PotentialType>::checkMove(const IngredientsType& ingredients, MoveLocalSc& move)
{
	
	//first calculate hypothetical new distance
	int32_t attribute=ingredients.getMolecules()[move.getIndex()].getAttributeTag();
	
	double testDistance;
	//NOTE: distance is pos, if polymer above bilayer, neg if below, therfore this works
	if(attribute==MonomerTypesCPU::LIPID_HEAD||attribute==MonomerTypesCPU::LIPID_TAIL)
	{
		testDistance=distanceBilayerGroup-double(move.getDir().getZ())/double(lipids.size());
		
	}
	else if(affectedTypes.count(attribute)>0)
	{
		testDistance=distanceBilayerGroup+double(move.getDir().getZ())/double(affectedMonomerGroup.size());
		
	}
	else return true;

	
	//the rest happens only if we have not returned yet, i.e. if distance has been calculated
	Lemonade::reduceDistanceInPeriodicSpace(testDistance,double(ingredients.getBoxZ()));

	double potentialDiff=potential(testDistance)-potential(distanceBilayerGroup);
	
	if(potentialDiff==std::numeric_limits<double>::infinity())
		return false;
	else if (potentialDiff==0.0)
		return true;
	else
	{
		move.multiplyProbability(std::exp(-potentialDiff));
		return true;
	}
		
}


template<class PotentialType>
template<class IngredientsType>
void FeatureBilayerDistancePotential<PotentialType>::applyMove(IngredientsType& ingredients, const MoveLocalSc& move)
{
	int32_t attribute=ingredients.getMolecules()[move.getIndex()].getAttributeTag();
	//NOTE: distance is pos, if polymer above bilayer, neg if below, therfore this works
	if(attribute==MonomerTypesCPU::LIPID_HEAD||attribute==MonomerTypesCPU::LIPID_TAIL)
	{
		distanceBilayerGroup-=double(move.getDir().getZ())/double(lipids.size());
		Lemonade::reduceDistanceInPeriodicSpace(distanceBilayerGroup,double(ingredients.getBoxZ()));
		
	}
	else if(affectedTypes.count(attribute)>0)
	{
		distanceBilayerGroup+=double(move.getDir().getZ())/double(affectedMonomerGroup.size());
		Lemonade::reduceDistanceInPeriodicSpace(distanceBilayerGroup,double(ingredients.getBoxZ()));	
	}
}


template<class PotentialType>
template<class IngredientsType>
void FeatureBilayerDistancePotential<PotentialType>::synchronize(IngredientsType& ingredients)
{
	//sort the monomers into groups
	lipids.clear();
	affectedMonomerGroup.clear();
	
	for(size_t n=0;n<ingredients.getMolecules().size();n++)
	{
		if(ingredients.getMolecules()[n].getAttributeTag()==MonomerTypesCPU::LIPID_HEAD || ingredients.getMolecules()[n].getAttributeTag()==MonomerTypesCPU::LIPID_TAIL)
		{
			lipids.push_back(n);
		}
                else if(affectedTypes.count(ingredients.getMolecules()[n].getAttributeTag())>0)
                {
			affectedMonomerGroup.push_back(n);
		}
	}
	
	if(lipids.size()>0 && affectedMonomerGroup.size()>0)
	{
		//calculate the distance between bilayer and affected monomer group
		//and check, if the potential there is not infinite. this would mean
		//that the system is in an unphysical state
		
		int32_t sumZ=0;
		std::cout<<"FeatureBilayerDistancePotential: affected monomers:";
		for(size_t n=0;n<affectedMonomerGroup.size();n++)
		{
			sumZ+=ingredients.getMolecules()[affectedMonomerGroup[n]].getZ();
			std::cout<<affectedMonomerGroup[n]<<" ";
		}
		std::cout<<std::endl;
		
		distanceBilayerGroup=double(sumZ)/double(affectedMonomerGroup.size());
		sumZ=0;
		for(size_t n=0;n<lipids.size();n++)
		{
			sumZ+=ingredients.getMolecules()[lipids[n]].getZ();	
		}
		distanceBilayerGroup-=double(sumZ)/double(lipids.size());
		
		
		Lemonade::reduceDistanceInPeriodicSpace(distanceBilayerGroup,double(ingredients.getBoxZ()));
		if(potential(distanceBilayerGroup)==std::numeric_limits< double >::infinity())
		{
			std::stringstream errormessage;
			errormessage<<"FeatureBilayerDistancePotential::synchronize()...conformation not compatible with bias potential\n";
			errormessage<<"the distance between bilayer mid-plane and polymer center of mass is "<<distanceBilayerGroup<<std::endl;
			throw std::runtime_error(errormessage.str());
		}
	}

}

#endif //FEATURE_BIAS_POTENTIAL_H
