#ifndef DISTANCE_TO_BILAYER_POTENTIALS_H
#define DISTANCE_TO_BILAYER_POTENTIALS_H

#include <limits>
#include <cmath>

#include <LeMonADE-Extensions/utilities/Histogram1D.h>

class DistanceToBilayerPotentialWell
{
public:
	DistanceToBilayerPotentialWell():lowerLimit(0.0),upperLimit(std::numeric_limits<double>::infinity()){}
	~DistanceToBilayerPotentialWell(){}
	
	
	double operator()(double distance)
	{
		double d=std::fabs(distance);
		if(d>=lowerLimit && d<=upperLimit)
			return 0.0;
		
		else return std::numeric_limits< double >::infinity();
	}
	
	void setPotentialWellBorders(double lower,double upper){lowerLimit=lower;upperLimit=upper;}
	
	
	
private:
	
	double upperLimit,lowerLimit;
};

class DistanceToBilayerHarmonicPotential
{
public:
	DistanceToBilayerHarmonicPotential():k(0.0),d0(0.0){}
	~DistanceToBilayerHarmonicPotential(){}
	
	
	double operator()(double distance)
	{
		//double d=std::fabs(distance);
		double d=distance;
		
		return k*(d-d0)*(d-d0);
	}
	
	void setSpringParameters(double springConstant,double minimumDist){k=springConstant;d0=minimumDist;}
	
private:
	
	double k,d0;
};

class DistanceToBilayerLinearPotential
{
public:
	DistanceToBilayerLinearPotential():a(0.0){}
	~DistanceToBilayerLinearPotential(){}
	
	
	double operator()(double distance)
	{
		double d=std::fabs(distance);
		
		return a*d;
	}
	
	void setLinearPotentialParameters(double slope){a=slope;}
	
private:
	
	double a;
};


class ExternalCosinePotential
{
public:
	ExternalCosinePotential():depth(1.0),period(64.0),pi(4.0*std::atan(1.0)){}
	~ExternalCosinePotential(){}
	
	double operator()(double distance)
	{
		return depth*std::cos(2*pi*distance/period);
	}
	
	void setPotentialParameters(double p,double d){period=p;depth=d;}
private:
	double depth;
	double pi;
	double period;
	
};


class DiscreteBinsPotential
{
public:
	DiscreteBinsPotential(){}
	~DiscreteBinsPotential(){}
	
	double operator()(double coordinate) const {return pmf.getCountAt(coordinate);}
	
	void setDiscretePotentialParameters(double min, double max, uint32_t nbins){
		pmf.reset(min,max,nbins);
	}
	
	double getPMFMinCoordinate() const {return pmf.getMinCoordinate();}
	double getPMFMaxCoordinate() const {return pmf.getMaxCoordinate();}
	uint32_t getNumberOfCoordinateBins() const {return pmf.getNBins();}
	void addValue(double coord,double value){pmf.addValue(coord,value);}
	std::vector<double> getPMFValues() const {return pmf.getVectorValues();}
	std::vector<double> getPMFCoordinates() const {return pmf.getVectorBins();}
private:
	Histogram1D pmf;
	
};

#endif //DISTANCE_TO_BILAYER_POTENTIAL_WELL_H